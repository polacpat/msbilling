package com.tieto.tds.domains.invoice.line;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CustomInvoiceLineTest {

	@Test
	public void test(){
		CustomInvoiceLine line = new CustomInvoiceLine();
		line.setAmount(1d);
		line.setCompanyid("111");
		line.setCostCenterId("222");
		line.setDateCreated(Calendar.getInstance().getTime());
		line.setId(1);
		line.setItemName("name");
		line.setLastUpdated(Calendar.getInstance().getTime());
		line.setNo("2017-01");
		line.setPrice(new BigDecimal(10));
		line.setProjectid("333");
		line.setUnit("pcs");
		
		assertThat(1d).isEqualTo(line.getAmount());
		assertThat("111").isEqualTo(line.getCompanyid());
		assertThat("222").isEqualTo(line.getCostCenterId());
		assertNotNull(line.getDateCreated());
		assertThat(1l).isEqualTo(line.getId());
		assertThat("name").isEqualTo(line.getItemName());
		assertNotNull(line.getLastUpdated());
		assertThat("2017-01").isEqualTo(line.getNo());
		assertThat(new BigDecimal(10)).isEqualTo(line.getPrice());
		assertThat("333").isEqualTo(line.getProjectid());
		assertThat("pcs").isEqualTo(line.getUnit());
	}

}
