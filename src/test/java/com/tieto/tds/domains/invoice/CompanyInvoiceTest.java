package com.tieto.tds.domains.invoice;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.line.InvoiceLine;

@RunWith(MockitoJUnitRunner.class)
public class CompanyInvoiceTest {

	@Test
	public void test(){
		CompanyInvoice invoice = new CompanyInvoice();
		invoice.setCompanyid("111");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		List<InvoiceLine> lines =  new ArrayList<InvoiceLine>();
		invoice.setLines(lines);
		invoice.setNumber("2017-01");

		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("2017-01").isEqualTo(invoice.getNumber());
		assertNotNull(invoice.getDateCreated());
		assertThat(0).isEqualTo(invoice.getLines().size());
		assertThat("111").isEqualTo(invoice.getCompanyid());
	}

	@Test
	public void testConstructor(){
		CompanyInvoice invoice = new CompanyInvoice("111","2017-01");
		assertThat("111").isEqualTo(invoice.getCompanyid());
		assertThat("2017-01").isEqualTo(invoice.getNumber());
	}
	
	@Test
	public void testToString(){
		CompanyInvoice invoice = new CompanyInvoice("111","2017-01");
		assertThat("ID:0, no: 2017-01, companyid: 111").isEqualTo(invoice.toString());
	}
}
