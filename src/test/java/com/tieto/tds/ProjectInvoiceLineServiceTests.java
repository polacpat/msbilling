package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.line.CustomInvoiceLine;
import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.dtos.company.project.app.StoreItemDto;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.dtos.company.project.server.ServerDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.ApplicationRepository;
import com.tieto.tds.repositories.CustomInvoiceLineRepository;
import com.tieto.tds.repositories.TenantUsageRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.UsageService;
import com.tieto.tds.services.commons.ResourceUsage;
import com.tieto.tds.services.impl.ProjectInvoiceLineServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class ProjectInvoiceLineServiceTests {

	@Mock
	private TenantUsageRepository tenantUsageRepository;

	@Mock
	private UsageService usageService;
	@Mock
	private ApplicationRepository applicationRepository;
	@Mock
	private InvoiceLineRepository invoiceLineRepository;
	@Mock
	private CustomInvoiceLineRepository customInvoiceLineRepository;

	@InjectMocks
	private ProjectInvoiceLineServiceImpl service = new ProjectInvoiceLineServiceImpl(tenantUsageRepository,
			usageService, applicationRepository, invoiceLineRepository, customInvoiceLineRepository);
	@Test
	public void test_getProjectResourcesInvoiceLines() {

		List<Usage> usage = new ArrayList<Usage>();
		when(tenantUsageRepository.getUsageForTenantInTimePeriod(anyString(), any(LocalDate.class), any(LocalDate.class))).thenReturn(usage);
		ResourceUsage resourceUsage = new ResourceUsage();
		resourceUsage.setCpu(new BigDecimal(2));
		resourceUsage.setHdd(new BigDecimal(2));
		resourceUsage.setImages(new BigDecimal(2));
		resourceUsage.setInstances(new BigDecimal(2));
		resourceUsage.setRam(new BigDecimal(2));
		resourceUsage.setSnapshots(new BigDecimal(2));
		resourceUsage.setVolumes(new BigDecimal(2));

		when(usageService.getTotalUsage(usage)).thenReturn(resourceUsage );

		List<ResourceDto> resources = new ArrayList<ResourceDto>();
		ResourceDto res1 = new ResourceDto();
		resources.add(res1);

		res1.setTenantid("333");
		FeeDto fee = new FeeDto();
		fee.setCpu(2);
		fee.setHdd(2);
		fee.setInstance(2);
		fee.setNetwork(2);
		fee.setRam(2);
		fee.setSsd(2);
		fee.setSsd(2);

		LocalDate start = LocalDate.ofYearDay(2017, 1);
		LocalDate end = LocalDate.now();

		List<InvoiceLineDto> invoiceLines = service.getProjectResourcesInvoiceLines(resources, fee , start, end);

		BigDecimal resultPrice = BigDecimal.ZERO;
		double resultAmount = 0;
		for (InvoiceLineDto line: invoiceLines){
			resultPrice = resultPrice.add(line.getPrice());
			resultAmount = resultAmount+line.getAmount();
		}

		assertThat(resultAmount ).isEqualTo(14d);
		assertThat(resultPrice).isEqualTo(BigDecimal.valueOf(14));
		assertThat(invoiceLines.size()).isEqualTo(7);
	}

	@Test
	public void test_getProjectApplicationCostInvoiceLines(){

		List<ApplicationDto> apps = new ArrayList<ApplicationDto>();
		ApplicationDto app1 = new ApplicationDto();
		app1.setAppid("444");
		app1.setCostPeriod("monthly");
		app1.setCostPrice(2d);
		StoreItemDto storeItem = new StoreItemDto();
		storeItem.setTitle("Gerrit");
		app1.setStoreItem(storeItem );
		ServerDto server = new ServerDto();
		server.setFqdn("gerrit.tds.com");
		app1.setServer(server );

		ApplicationDto app2 = new ApplicationDto();
		app2.setAppid("555");
		app2.setCostPeriod("monthly");
		app2.setCostPrice(2d);
		app2.setStoreItem(storeItem );
		app2.setServer(server );

		apps.add(app1);
		apps.add(app2);
		when(applicationRepository.findAllActiveAppsByProjectidAndPeriod(anyString(), anyString(), any(LocalDate.class), any(LocalDate.class))).thenReturn(apps );

		LocalDate start = LocalDate.ofYearDay(2017, 1);
		LocalDate end = LocalDate.now();
		List<InvoiceLineDto> invoices = service.getProjectApplicationCostInvoiceLines("111", "222", start, end);

		BigDecimal resultPrice = BigDecimal.ZERO;
		double resultAmount = 0;
		for (InvoiceLineDto line: invoices){
			resultPrice = resultPrice.add(line.getPrice());
			resultAmount = resultAmount+line.getAmount();
		}

		assertThat(resultPrice).isEqualTo(BigDecimal.valueOf(4.0));
		assertThat(resultAmount ).isEqualTo(2d);
		assertThat(invoices.size() ).isEqualTo(2);
	}

	@Test
	public void test_getProjectCustomInvoiceLines(){

		List<CustomInvoiceLine> customInvoiceLines = new ArrayList<CustomInvoiceLine>();
		CustomInvoiceLine line1 = new CustomInvoiceLine();
		line1.setAmount(2d);
		line1.setItemName("test");
		line1.setPrice(new BigDecimal(2));
		line1.setUnit("pcs");
		
		CustomInvoiceLine line2 = new CustomInvoiceLine();
		line2.setAmount(2d);
		line2.setItemName("pokus");
		line2.setPrice(new BigDecimal(2));
		line2.setUnit("pcs");
		
		customInvoiceLines.add(line1);
		customInvoiceLines.add(line2);
		when(customInvoiceLineRepository.findAllByNoAndProjectid(anyString(), anyString())).thenReturn(customInvoiceLines);

		LocalDate start = LocalDate.ofYearDay(2017, 1);
		LocalDate end = LocalDate.now();
		List<InvoiceLineDto> invoices = service.getProjectCustomInvoiceLines("111", "222", start, end);
		
		BigDecimal resultPrice = BigDecimal.ZERO;
		double resultAmount = 0;
		for (InvoiceLineDto line: invoices){
			resultPrice = resultPrice.add(line.getPrice());
			resultAmount = resultAmount+line.getAmount();
		}

		assertThat(resultPrice).isEqualTo(BigDecimal.valueOf(4));
		assertThat(resultAmount ).isEqualTo(4d);
		assertThat(invoices.size() ).isEqualTo(2);
	}
}
