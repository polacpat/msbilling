package com.tieto.tds.controllers;

import static org.hamcrest.Matchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.dtos.invoicing.CustomInvoiceLineDto;
import com.tieto.tds.services.CustomInvoiceLineService;
import com.tieto.tds.services.commons.LocalDateHolder;

public class ProjectCustomInvoiceLineControllerTest {

	private ObjectMapper objectMapperLocal = new ObjectMapper();

	MockMvc mockMvc;

	@Mock
	private CustomInvoiceLineService customInvoiceLineService;
	
	@InjectMocks
	private ProjectCustomInvoiceLineController controller = new ProjectCustomInvoiceLineController(customInvoiceLineService);
	
	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();
	}

	@Test
	public void thatCreateCustomInvoiceLineAPIReturns200() throws Exception {
		CustomInvoiceLineDto line = new CustomInvoiceLineDto();
		when(customInvoiceLineService.createCustomInvoiceLineForProject(anyString(), anyString(), any(CustomInvoiceLineDto.class)))
		.thenReturn(line );


		String json = objectMapperLocal.writeValueAsString(line);

		mockMvc.perform(
				post("/company/111/project/222/customInvoiceLine")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json))
				.andExpect(status().isOk());
	}

	@Test
	public void thatGetCustomInvoiceLinesAPIReturns200() throws Exception {
		mockMvc.perform(get("/company/111/project/222/customInvoiceLine"))
		.andDo(print())
		.andExpect(status().isOk());
	}

}
