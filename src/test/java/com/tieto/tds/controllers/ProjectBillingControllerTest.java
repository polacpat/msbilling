package com.tieto.tds.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;

import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.ProjectInvoiceService;
import com.tieto.tds.services.commons.LocalDateHolder;

public class ProjectBillingControllerTest {

	MockMvc mockMvc;
	@Mock
	private ProjectInvoiceService projectInvoiceService;
	@Mock
	private LocalDateUtilService localDateUtilService;

	@InjectMocks
	private ProjectBillingController controller = new ProjectBillingController(
			projectInvoiceService, localDateUtilService);
	

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();
	}

	@Test
	public void thatgenerateinvoicesAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(null, null)).thenReturn(value );
		mockMvc.perform(post("/company/111/project/222/invoice"))
		.andDo(print())
		.andExpect(status().isOk());

	}

	@Test
	public void thatGetProjectInvoiceAPIReturns200() throws Exception {
		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		when(projectInvoiceService.findOne(anyString(), anyString(), anyLong())).thenReturn(invoice);
		mockMvc.perform(get("/company/111/project/222/invoice/333"))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	public void thatGetProjectInvoicesAPIReturns200() throws Exception {
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		when(projectInvoiceService.findAllByProjectid(anyString(), anyString(), any(PageableMs.class))).thenReturn(page );
		mockMvc.perform(get("/company/111/project/222/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}


	@Test
	public void thatGetCompanyProjectInvoicesAPIReturns200() throws Exception {
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		when(projectInvoiceService.findAllByProjectid(anyString(), anyString(), any(PageableMs.class))).thenReturn(page );
		mockMvc.perform(get("/company/111/project/invoice"))
		.andDo(print())
		.andExpect(status().isOk());
	}
}
