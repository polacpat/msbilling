package com.tieto.tds.controllers;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.io.ByteArrayOutputStream;
import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import com.tieto.tds.repositories.BackendRepository;
import com.tieto.tds.services.AdminService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.MigrationService;
import com.tieto.tds.services.commons.LocalDateHolder;
import com.tieto.tds.services.reports.ExportInvoiceService;

public class AdminControllerTest {

	MockMvc mockMvc;

	@Mock
	private AdminService adminService;
	@Mock
	private ExportInvoiceService exportInvoiceService;
	@Mock
	private MigrationService migrationService;
	@Mock
	private BackendRepository backendRepository;
	@Mock
	private LocalDateUtilService localDateUtilService;

	@InjectMocks
	private AdminController controller = new AdminController(
			adminService, exportInvoiceService, migrationService, backendRepository, localDateUtilService);

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);

		mockMvc = standaloneSetup(controller).build();
	}

	@Test
	public void thatCustomJenkinsAPIReturns200() throws Exception {
		mockMvc.perform(get("/admin/customjenkins"))
		.andDo(print())
		.andExpect(status().isOk());
	}
	
	@Test
	public void thatgenerateinvoicesAPIReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(null, null)).thenReturn(value );
		mockMvc.perform(post("/admin/generateinvoices"))
		.andDo(print())
		.andExpect(status().isOk());

	}

	@Test
	public void thatgenerateinvoicesAPIWithFromParamReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(any(LocalDate.class), any(null))).thenReturn(value );
		
		mockMvc.perform(post("/admin/generateinvoices?from=2017-11-01"))
		.andDo(print())
		.andExpect(status().isOk());

	}

	@Test
	public void thatgenerateinvoicesAPIWithFromAndtoParamReturns200() throws Exception {
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		LocalDateHolder value = new LocalDateHolder(periodFrom, periodTo);
		when(localDateUtilService.getFromAndToFromInput(any(LocalDate.class), any(LocalDate.class))).thenReturn(value );

		mockMvc.perform(post("/admin/generateinvoices?from=2017-11-01&to=2017-12-01"))
		.andDo(print())
		.andExpect(status().isOk());

	}
	
	@Test(expected = NestedServletException.class) 
	public void thatgenerateinvoicesAPIWithFromAndtoParamReturns400() throws Exception {
		mockMvc.perform(post("/admin/generateinvoices?from=2017-12-01&to=2017-11-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatExportInvoicesAPIReturns200() throws Exception {
		ByteArrayOutputStream value = new ByteArrayOutputStream();
		when(exportInvoiceService.exportInvoicesByPeriod(anyString())).thenReturn(value);
		mockMvc.perform(get("/admin/exportinvoices?number=2017-01"))
		.andDo(print())
		.andExpect(status().isOk());
	}

	@Test
	public void thatMigrateInvoicesAPIReturns200() throws Exception {
		mockMvc.perform(get("/admin/migrateinvoices"))
		.andDo(print())
		.andExpect(status().isOk());
	}


	@Test
	public void thatLoginAPIReturns200() throws Exception {
		mockMvc.perform(get("/admin/login"))
		.andDo(print())
		.andExpect(status().isOk());
	}

}
