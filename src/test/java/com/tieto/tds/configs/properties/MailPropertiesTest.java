package com.tieto.tds.configs.properties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.configs.properties.MailProperties.Report;

@RunWith(MockitoJUnitRunner.class)
public class MailPropertiesTest {

	@Test
	public void test(){
		MailProperties prop = new MailProperties();
		prop.setHost("host");
		Report report = new Report();
		String[] receivers = new String[]{"one"};
		report.setReceivers(receivers);
		prop.setReport(report);
		
		assertThat("host").isEqualTo(prop.getHost());
		assertThat(1).isEqualTo(prop.getReport().getReceivers().length);
	}
}
