package com.tieto.tds.configs.properties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BillingPropertiesTest {

	@Test
	public void test(){
		BillingProperties prop = new BillingProperties();
		prop.setReportfolder("/temp");
		
		assertThat("/temp").isEqualTo(prop.getReportfolder());
	}
}
