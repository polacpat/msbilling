package com.tieto.tds.configs.properties;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BackendPropertiesTest {

	@Test
	public void test(){
		BackendProperties prop = new BackendProperties();
		prop.setFqdn("fqdn");
		prop.setPassword("pwd");
		prop.setSchema("schema");
		prop.setUseorchestration(true);
		prop.setUsername("username");

		assertThat("fqdn").isEqualTo(prop.getFqdn());
		assertThat("pwd").isEqualTo(prop.getPassword());
		assertThat("schema").isEqualTo(prop.getSchema());
		assertThat(true).isEqualTo(prop.isUseorchestration());
		assertThat("username").isEqualTo(prop.getUsername());
	}
}
