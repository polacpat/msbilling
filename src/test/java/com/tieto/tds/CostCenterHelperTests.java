package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.AurBillingDto;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;
import com.tieto.tds.services.CostCenterHelper;
import com.tieto.tds.services.impl.CostCenterHelperImpl;

@RunWith(MockitoJUnitRunner.class)
public class CostCenterHelperTests {

	CostCenterHelper service = new CostCenterHelperImpl();

	@Test
	public void test_isAplicableForCCInvoice_returnTrue(){

		AurApp app1 = new AurApp();
		app1.setFqdn("saas.tds.com");
		app1.setAppid("444");
		AurBillingDto billing = new AurBillingDto();
		billing.setType(SaasFeeType.PER_USER);
		billing.setValue(2d);
		app1.setBilling(billing );

		boolean aplicableForCCInvoice = service.isAplicableForCCInvoice(app1);

		assertThat(aplicableForCCInvoice ).isEqualTo(true);

	}

	@Test
	public void test_isAplicableForCCInvoice_returnFalse(){

		AurApp app1 = new AurApp();
		app1.setFqdn("saas.tds.com");
		app1.setAppid("444");
		AurBillingDto billing = new AurBillingDto();
		billing.setType(SaasFeeType.PER_SERVICE);
		billing.setValue(2d);
		app1.setBilling(billing );

		boolean aplicableForCCInvoice = service.isAplicableForCCInvoice(app1);

		assertThat(aplicableForCCInvoice ).isEqualTo(false);

	}

	@Test
	public void test_isAplicableForCCInvoice_returnFalseWithNullBillingSet(){

		AurApp app1 = new AurApp();
		app1.setFqdn("saas.tds.com");
		app1.setAppid("444");

		boolean aplicableForCCInvoice = service.isAplicableForCCInvoice(app1);

		assertThat(aplicableForCCInvoice ).isEqualTo(false);

	}
}
