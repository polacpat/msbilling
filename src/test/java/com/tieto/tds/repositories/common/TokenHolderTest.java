package com.tieto.tds.repositories.common;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;

import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TokenHolderTest {

	
	@Test
	public void test_loginResponseAvailable(){
		LoginResponse response = new LoginResponse();
		response.setExpiration(Calendar.getInstance().getTime());
		response.setToken("123");
		TokenHolder holder = new TokenHolderImpl();
		holder.setLoginResponse(response);
		
		assertThat(holder.getToken(), is("123"));
		assertThat(holder.getTokenExpiration(), greaterThan(0l));
	}
	
	@Test
	public void test_loginResponseNotAvailable(){
		TokenHolder holder = new TokenHolderImpl();
		
		assertThat(holder.getToken(), is(""));
		assertThat(holder.getTokenExpiration(), is(0l));
	}
}
