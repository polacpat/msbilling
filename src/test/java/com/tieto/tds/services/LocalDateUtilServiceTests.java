package com.tieto.tds.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.services.commons.LocalDateHolder;
import com.tieto.tds.services.impl.LocalDateUtilServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class LocalDateUtilServiceTests {

	@Test
	public void thatGetFromAndToFromInput_ReturnsdataForOnlyFromDate(){

		LocalDateUtilService service = new LocalDateUtilServiceImpl();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateFrom = LocalDate.parse("2017-11-01", formatter);
		LocalDate dateTo = LocalDate.parse("2017-12-01", formatter);

		LocalDateHolder fromAndToFromInput = service.getFromAndToFromInput(dateFrom, null);
		
		assertThat(dateFrom.getMonth()).isEqualTo(fromAndToFromInput.getFrom().getMonth());
		assertThat(dateFrom.getDayOfMonth()).isEqualTo(fromAndToFromInput.getFrom().getDayOfMonth());
		assertThat(dateFrom.getYear()).isEqualTo(fromAndToFromInput.getFrom().getYear());
		assertThat(dateTo.getMonth()).isEqualTo(fromAndToFromInput.getTo().getMonth());
		assertThat(dateTo.getDayOfMonth()).isEqualTo(fromAndToFromInput.getTo().getDayOfMonth());
		assertThat(dateTo.getYear()).isEqualTo(fromAndToFromInput.getTo().getYear());
	}
	
	@Test
	public void thatGetFromAndToFromInput_ReturnsdataForOnlyToDate(){

		LocalDateUtilService service = new LocalDateUtilServiceImpl();

		LocalDate dateFrom = LocalDate.now();
		dateFrom = dateFrom.withDayOfMonth(1);
		LocalDate dateTo = dateFrom.plusMonths(1);

		LocalDateHolder fromAndToFromInput = service.getFromAndToFromInput(null, dateTo);
		
		assertThat(dateFrom.getMonth()).isEqualTo(fromAndToFromInput.getFrom().getMonth());
		assertThat(dateFrom.getDayOfMonth()).isEqualTo(fromAndToFromInput.getFrom().getDayOfMonth());
		assertThat(dateFrom.getYear()).isEqualTo(fromAndToFromInput.getFrom().getYear());
		assertThat(dateTo.getMonth()).isEqualTo(fromAndToFromInput.getTo().getMonth());
		assertThat(dateTo.getDayOfMonth()).isEqualTo(fromAndToFromInput.getTo().getDayOfMonth());
		assertThat(dateTo.getYear()).isEqualTo(fromAndToFromInput.getTo().getYear());
	}

	@Test
	public void thatGetFromAndToFromInput_ReturnsdataForBothDays(){

		LocalDateUtilService service = new LocalDateUtilServiceImpl();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate dateFrom = LocalDate.parse("2017-11-01", formatter);
		LocalDate dateTo = LocalDate.parse("2017-12-01", formatter);

		LocalDateHolder fromAndToFromInput = service.getFromAndToFromInput(dateFrom, dateTo);
		
		assertThat(dateFrom.getMonth()).isEqualTo(fromAndToFromInput.getFrom().getMonth());
		assertThat(dateFrom.getDayOfMonth()).isEqualTo(fromAndToFromInput.getFrom().getDayOfMonth());
		assertThat(dateFrom.getYear()).isEqualTo(fromAndToFromInput.getFrom().getYear());
		assertThat(dateTo.getMonth()).isEqualTo(fromAndToFromInput.getTo().getMonth());
		assertThat(dateTo.getDayOfMonth()).isEqualTo(fromAndToFromInput.getTo().getDayOfMonth());
		assertThat(dateTo.getYear()).isEqualTo(fromAndToFromInput.getTo().getYear());
	}
	
	@Test
	public void thatGetFromAndToFromInput_ReturnsdataForNulls(){

		LocalDateUtilService service = new LocalDateUtilServiceImpl();

		LocalDate dateFrom = LocalDate.now();
		dateFrom = dateFrom.withDayOfMonth(1);
		LocalDate dateTo = dateFrom.plusMonths(1);

		LocalDateHolder fromAndToFromInput = service.getFromAndToFromInput(null, null);
		
		assertThat(dateFrom.getMonth()).isEqualTo(fromAndToFromInput.getFrom().getMonth());
		assertThat(dateFrom.getDayOfMonth()).isEqualTo(fromAndToFromInput.getFrom().getDayOfMonth());
		assertThat(dateFrom.getYear()).isEqualTo(fromAndToFromInput.getFrom().getYear());
		assertThat(dateTo.getMonth()).isEqualTo(fromAndToFromInput.getTo().getMonth());
		assertThat(dateTo.getDayOfMonth()).isEqualTo(fromAndToFromInput.getTo().getDayOfMonth());
		assertThat(dateTo.getYear()).isEqualTo(fromAndToFromInput.getTo().getYear());
	}
}
