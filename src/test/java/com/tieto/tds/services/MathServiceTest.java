package com.tieto.tds.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.services.impl.MathServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class MathServiceTest {

	@Test
	public void testTotalPriceWithNullLinesHalfDownUp(){
		MathServiceImpl service = new MathServiceImpl();

		BigDecimal totalPrice = new BigDecimal("122.31499");
		BigDecimal value = service.getRoundedNumber(totalPrice);
		
		assertThat(new BigDecimal("122.31")).isEqualTo(value );
	}

	@Test
	public void testTotalPriceWithNullLinesHalfDownDown(){
		MathServiceImpl service = new MathServiceImpl();

		BigDecimal totalPrice = new BigDecimal("122.31501");
		BigDecimal value = service.getRoundedNumber(totalPrice);
		
		assertThat(new BigDecimal("122.32")).isEqualTo(value );
	}
}
