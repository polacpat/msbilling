package com.tieto.tds.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.domains.invoice.Invoice;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.impl.InvoiceLineServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class InvoiceLineServiceTests {
	
	@Mock
	private InvoiceLineRepository invoiceLineRepository;
	 
	@InjectMocks
	public InvoiceLineServiceImpl service = new InvoiceLineServiceImpl(invoiceLineRepository);
	
	@Test
	public void that_getTotalPriceByInvoiceID_returncorrectTotalPrice(){
		List<InvoiceLine> lines = new ArrayList<>();
		Invoice invoice = new ProjectInvoice();
		InvoiceLine line1 = new InvoiceLine(invoice , "cpu", "pcs", 100.0, new BigDecimal(0.12345));
		InvoiceLine line2 = new InvoiceLine(invoice , "hdd", "pcs", 100.0, new BigDecimal(0.23456));
		lines.add(line1);
		lines.add(line2);
		when(invoiceLineRepository.findAllByInvoiceId(anyLong())).thenReturn(lines);

		BigDecimal totalPrice = service.getTotalPriceByInvoiceID(1l);

		assertEquals(new BigDecimal("35.80100"),totalPrice.setScale(5, RoundingMode.CEILING));

	}
}
