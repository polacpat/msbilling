package com.tieto.tds.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.services.impl.UrlServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UrlServiceTest {

	@Mock
	private PortalProperties properties;

	@InjectMocks
	private UrlServiceImpl service = new UrlServiceImpl(properties);

	@Test
	public void testGetProjectUrl(){

		when(properties.getSchema()).thenReturn("http");
		when(properties.getFqdn()).thenReturn("some.fqdn.com");
		when(properties.getContext()).thenReturn("/portal");

		ProjectDto project = new ProjectDto();
		project.setProductId("222");
		CompanyDto company = new CompanyDto();
		company.setAreaid("111");
		project.setCompany(company );
		String url = service.getProjectUrl(project);

		assertThat("http://some.fqdn.com/portal/#/areas/111/projects/222/billing").isEqualTo(url);
	}

	@Test
	public void testGetCostCenterUrl(){

		when(properties.getSchema()).thenReturn("http");
		when(properties.getFqdn()).thenReturn("some.fqdn.com");
		when(properties.getContext()).thenReturn("/portal");

		CostCenterDto costCenter = new CostCenterDto();
		costCenter.setId(1);
		CompanyDto company = new CompanyDto();
		company.setAreaid("111");
		costCenter.setCompany(company );
		String url = service.getCostCenterUrl(costCenter);

		assertThat("http://some.fqdn.com/portal/#/areas/111/ccinvoices/1/billing").isEqualTo(url);
	}
	
	@Test
	public void testGetCostCenterUrlInNA(){

		when(properties.getSchema()).thenReturn("http");
		when(properties.getFqdn()).thenReturn("some.fqdn.com");
		when(properties.getContext()).thenReturn("/portal");

		CostCenterDto costCenter = new CostCenterDto();
		costCenter.setId(1);
		String url = service.getCostCenterUrl(costCenter);

		assertThat("http://some.fqdn.com/portal/#/areas/NA/ccinvoices/1/billing").isEqualTo(url);
	}

	@Test
	public void testGetCompanyUrl(){

		when(properties.getSchema()).thenReturn("http");
		when(properties.getFqdn()).thenReturn("some.fqdn.com");
		when(properties.getContext()).thenReturn("/portal");

		CompanyDto  company = new CompanyDto ();
		company.setAreaid("111");
		String url = service.getCompanyUrl(company);

		assertThat("http://some.fqdn.com/portal/#/areas/111/billing").isEqualTo(url);
	}
	
}
