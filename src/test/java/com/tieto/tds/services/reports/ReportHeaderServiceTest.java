package com.tieto.tds.services.reports;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.AurBillingDto;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;

@RunWith(MockitoJUnitRunner.class)
public class ReportHeaderServiceTest {

	@Test
	public void thatCreateUsageHeaderReturnRow(){
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFRow row = workbook.createSheet().createRow(0);
		ReportHeaderServiceImpl service = new ReportHeaderServiceImpl();

		Row updatedRow = service.createUsageHeader(row);

		assertThat("Project").isEqualTo(updatedRow.getCell(0).getStringCellValue());
		assertThat("ID").isEqualTo(updatedRow.getCell(1).getStringCellValue());
		assertThat("CPU").isEqualTo(updatedRow.getCell(2).getStringCellValue());
		assertThat("RAM").isEqualTo(updatedRow.getCell(3).getStringCellValue());
		assertThat("DISK").isEqualTo(updatedRow.getCell(4).getStringCellValue());
		assertThat("INSTANCES").isEqualTo(updatedRow.getCell(5).getStringCellValue());
		assertThat("SNAPSHOTS").isEqualTo(updatedRow.getCell(6).getStringCellValue());
		assertThat("IMAGES").isEqualTo(updatedRow.getCell(7).getStringCellValue());
		assertThat("VOLUMES").isEqualTo(updatedRow.getCell(8).getStringCellValue());
		assertThat("Errors").isEqualTo(updatedRow.getCell(9).getStringCellValue());
	}

	@Test
	public void thatCreateProjectHeaderReturnRow(){
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFRow row = workbook.createSheet().createRow(0);
		ReportHeaderServiceImpl service = new ReportHeaderServiceImpl();

		Row updatedRow = service.createProjectHeader(row);

		assertThat("Project").isEqualTo(updatedRow.getCell(0).getStringCellValue());
		assertThat("ID").isEqualTo(updatedRow.getCell(1).getStringCellValue());
		assertThat("Cost center").isEqualTo(updatedRow.getCell(2).getStringCellValue());
		assertThat("Total price").isEqualTo(updatedRow.getCell(3).getStringCellValue());
		assertThat("Tier").isEqualTo(updatedRow.getCell(4).getStringCellValue());
		assertThat("Company").isEqualTo(updatedRow.getCell(5).getStringCellValue());
		assertThat("Link").isEqualTo(updatedRow.getCell(6).getStringCellValue());
		assertThat("Errors").isEqualTo(updatedRow.getCell(7).getStringCellValue());
	}

	@Test
	public void thatCreateHeaderForCostCentersReturnRow(){
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFRow row = workbook.createSheet().createRow(0);
		ReportHeaderServiceImpl service = new ReportHeaderServiceImpl();

		Row updatedRow = service.createHeaderForCostCenters(row);

		assertThat("Cost center").isEqualTo(updatedRow.getCell(0).getStringCellValue());
		assertThat("Company").isEqualTo(updatedRow.getCell(1).getStringCellValue());
		assertThat("Total price").isEqualTo(updatedRow.getCell(2).getStringCellValue());
		assertThat("Link").isEqualTo(updatedRow.getCell(3).getStringCellValue());
	}

	@Test
	public void thatCreateHeaderForCompanyReturnRow(){
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFRow row = workbook.createSheet().createRow(0);
		ReportHeaderServiceImpl service = new ReportHeaderServiceImpl();

		Row updatedRow = service.createHeaderForCompany(row);

		assertThat("Title").isEqualTo(updatedRow.getCell(0).getStringCellValue());
		assertThat("Total price").isEqualTo(updatedRow.getCell(1).getStringCellValue());
		assertThat("Link").isEqualTo(updatedRow.getCell(2).getStringCellValue());
	}

	@Test
	public void thatCreateCostCenterFullReportHeaderWorks(){
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFRow row = workbook.createSheet().createRow(0);
		Row subheader = workbook.createSheet().createRow(1);

		ReportHeaderServiceImpl service = new ReportHeaderServiceImpl();

		List<AurApp> aurApps = new ArrayList<>();
		AurApp app1 = new AurApp();
		app1.setAppid("111");
		app1.setFqdn("fqdn1.com");
		app1.setName("Jira");
		AurBillingDto billing = new AurBillingDto();
		billing.setType(SaasFeeType.PER_USER);
		billing.setUserSuppoertFee(0d);
		billing.setValue(3d);
		app1.setBilling(billing );
		aurApps.add(app1);
		CompanyDto company = new CompanyDto();
		company.setUserFee(3d);

		service.createCostCenterFullReportHeader(row, subheader, company, aurApps);;

		assertThat("Cost Center").isEqualTo(row.getCell(0).getStringCellValue());
		assertThat("Users").isEqualTo(row.getCell(1).getStringCellValue());
		assertThat("fqdn1.com").isEqualTo(row.getCell(2).getStringCellValue());

		assertThat(3d).isEqualTo(subheader.getCell(2).getNumericCellValue());
	}


}
