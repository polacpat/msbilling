package com.tieto.tds;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class TimeTests {

	@Test
	public void testTime(){

		LocalDateTime fromDateTime = LocalDateTime.of(2017, 10, 01, 0, 0, 0);
		LocalDateTime toDateTime = LocalDateTime.of(2017, 10, 02, 0, 0, 0);

		long seconds = fromDateTime.until( toDateTime, ChronoUnit.SECONDS);

		assertEquals(86400, seconds);
	}
}
