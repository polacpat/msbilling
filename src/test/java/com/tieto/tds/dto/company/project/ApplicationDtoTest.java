package com.tieto.tds.dto.company.project;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.dtos.company.project.app.StoreItemDto;
import com.tieto.tds.dtos.company.project.server.ServerDto;
import com.tieto.tds.dtos.user.UserDto;

@RunWith(MockitoJUnitRunner.class)
public class ApplicationDtoTest {

	@Test
	public void testNoNulls(){
		ApplicationDto application = new ApplicationDto();

		assertNotNull(application.getStoreItem());
		assertNotNull(application.getConnections());
		assertNotNull(application.getServer());
		assertNotNull(application.getCreatedBy());
		assertNotNull(application.getDeletedBy());
	}

	@Test
	public void testMethods(){
		ApplicationDto application = new ApplicationDto();
		application.setAppid("111");
		application.setAuroraStatus("status");
		application.setConnections(new ArrayList<>());
		application.setCostPeriod("month");
		application.setCostPrice(2d);
		application.setCreatedBy(new UserDto());
		application.setDateCreated(Calendar.getInstance().getTime());
		application.setDateDeleted(Calendar.getInstance().getTime());
		application.setDeletedBy(new UserDto());
		application.setServer(new ServerDto());
		application.setShared(true);
		application.setStatus("running");
		application.setStoreItem(new StoreItemDto());

		assertNotNull(application.getStoreItem());
		assertNotNull(application.getConnections());
		assertNotNull(application.getServer());
		assertNotNull(application.getCreatedBy());
		assertNotNull(application.getDeletedBy());
		assertNotNull(application.getDateCreated());
		assertNotNull(application.getDateDeleted());
		assertThat(application.getAppid(), is("111"));
		assertThat(application.getAuroraStatus(), is("status"));
		assertThat(application.getCostPeriod(), is("month"));
		assertThat(application.getCostPrice(), is(2d));
		assertThat(application.isShared(), is(true));
		assertThat(application.getStatus(), is("running"));

	}

	@Test
	public void testToString(){
		ApplicationDto application = new ApplicationDto();
		application.setAppid("111");
		StoreItemDto storeItem = new StoreItemDto();
		storeItem.setTitle("title");
		application.setStoreItem(storeItem);

		assertThat(application.toString(), is("ID: 111, title: title"));
	}
	
	@Test
	public void testToStringNullStoreItem(){
		ApplicationDto application = new ApplicationDto();
		application.setAppid("111");
		
		assertThat(application.toString(), is("ID: 111, title: N/A"));
	}
}
