package com.tieto.tds.dto.company.project;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.Calendar;
import java.util.HashSet;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.company.project.TierDto;
import com.tieto.tds.dtos.user.UserDto;

@RunWith(MockitoJUnitRunner.class)
public class ProjectDtoTest {

	@Test
	public void testNoNulls(){
		ProjectDto project = new ProjectDto();

		assertNotNull(project.getCompany());
		assertNotNull(project.getTier());
		assertNotNull(project.getCostCenter());
		assertNotNull(project.getCreatedBy());
		assertNotNull(project.getDeletedBy());
	}

	@Test
	public void testNoNulls2(){
		ProjectDto project = new ProjectDto();
		project.setCompany(new CompanyDto());
		project.setTier(new TierDto());
		project.setCostCenter(new CostCenterDto());
		project.setCreatedBy(new UserDto());
		project.setDeletedBy(new UserDto());

		assertNotNull(project.getCompany());
		assertNotNull(project.getTier());
		assertNotNull(project.getCostCenter());
		assertNotNull(project.getCreatedBy());
		assertNotNull(project.getDeletedBy());
	}

	@Test
	public void testMethods(){
		ProjectDto project = new ProjectDto();
		project.setActive(true);
		project.setCompanyid("111");
		project.setCreatedBy(new UserDto());
		project.setCompany(new CompanyDto());
		project.setTier(new TierDto());
		project.setCostCenter(new CostCenterDto());
		project.setCreateResource(true);
		project.setDateCreated(Calendar.getInstance().getTime());
		project.setDateDeleted(Calendar.getInstance().getTime());
		project.setDeletedBy(new UserDto());
		project.setDescription("desc");
		project.setId(2);
		project.setMassManagement(true);
		project.setName("name");
		project.setProductId("222");
		project.setRichDescription("rich");
		project.setTier(new TierDto());
		project.setUiElementIDs(new HashSet<>());

		assertThat(project.getCompanyid(), is("111"));
		assertThat(project.isCreateResource(), is(true));
		assertNotNull(project.getDateCreated());
		assertNotNull(project.getDateDeleted());
		assertNotNull(project.getUiElementIDs());
		assertThat(project.getDescription(), is("desc"));
		assertThat(project.getName(), is("name"));
		assertThat(project.getProductId(), is("222"));
		assertThat(project.getRichDescription(), is("rich"));
		assertThat(project.isMassManagement(), is(true));
		assertThat(project.isActive(), is(true));
		assertThat(project.getId(), is(2l));
		assertThat(project.toString(), is("PS dto = name - 222, desc" ));

	}

	@Test
	public void testMethodsNullDates(){
		ProjectDto project = new ProjectDto();

		assertThat(project.getDateCreated(), nullValue());
		assertThat(project.getDateDeleted(), nullValue());
		assertThat(project.getDescription(), is(""));
	}

}
