package com.tieto.tds.dto.company.project;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Calendar;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.project.resource.CapacityDto;
import com.tieto.tds.dtos.company.project.server.OsDto;
import com.tieto.tds.dtos.company.project.server.ServerDto;
import com.tieto.tds.dtos.user.UserDto;

@RunWith(MockitoJUnitRunner.class)
public class ServerDtoTest {

	@Test
	public void testFullObject(){
		ServerDto server = new ServerDto();
		server.setAuthentication("ssh");
		server.setServerid("111");
		server.setStatus("running");
		server.setFqdn("server.proj.comp.com");
		server.setManageable(true);
		server.setDateCreated(Calendar.getInstance().getTime());
		server.setCapacity(new CapacityDto());
		server.setCreatedBy(new UserDto());
		server.setOs(new OsDto());
		server.setApplications(new ArrayList<>());
		server.setConnections(new ArrayList<>());

		assertThat(server.getAuthentication(), is("ssh"));
		assertThat(server.getFqdn(), is("server.proj.comp.com"));
		assertThat(server.getServerid(), is("111"));
		assertThat(server.getStatus(), is("running"));
		assertThat(server.isManageable(), is(true));

		assertNotNull(server.getDateCreated());
		assertNotNull(server.getApplications());
		assertNotNull(server.getConnections());
		assertNotNull(server.getOs());
		assertNotNull(server.getCapacity());
		assertNotNull(server.getCreatedBy());
	}

	@Test
	public void testNoNullReturned(){
		ServerDto server = new ServerDto();

		assertThat(server.getDateCreated(), nullValue());
		assertNotNull(server.getApplications());
		assertNotNull(server.getConnections());
		assertNotNull(server.getOs());
		assertNotNull(server.getCapacity());
		assertNotNull(server.getCreatedBy());

	}

	@Test
	public void testHostName(){
		ServerDto server = new ServerDto();
		server.setFqdn("server.project.company.com");

		assertThat(server.getHostname(), is("server"));
	}

	@Test
	public void testHostNameNull(){
		ServerDto server = new ServerDto();

		assertThat(server.getHostname(), is("N/A"));
	}

	@Test
	public void testHostNameEmpty(){
		ServerDto server = new ServerDto();
		server.setFqdn("");

		assertThat(server.getHostname(), is("N/A"));
	}

	@Test
	public void testHostNameError(){
		ServerDto server = new ServerDto();
		server.setFqdn("server");

		assertThat(server.getHostname(), is("N/A"));
	}

	@Test
	public void testHostNameError2(){
		ServerDto server = new ServerDto();
		server.setFqdn(".");

		assertThat(server.getHostname(), is("N/A"));
	}


}
