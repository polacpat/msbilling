package com.tieto.tds;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.tieto.tds.dtos.company.UserRoleDto;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.services.impl.UserServiceImpl;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTests {

	@Mock
	private UserRepository userRepository;

	@InjectMocks
	UserServiceImpl service = new UserServiceImpl(userRepository);

	@Test
	public void test_getNonAdminUsersCount(){
		String adminUsername = "admin";
		String nonadminUsername = "johndoe";
		String nonadminUsername2 = "janedoe";

		
		List<UserDto> companyUsers = new ArrayList<UserDto>();
		UserDto u1 = new UserDto();
		u1.setUserToken(adminUsername);
		Set<UserRoleDto> globalRoles = new HashSet<UserRoleDto>();
		UserRoleDto pdcAdmin = new UserRoleDto();
		pdcAdmin.setAlternatekey("pdcadmin");
		globalRoles.add(pdcAdmin);
		u1.setGlobalRoles(globalRoles );
		companyUsers.add(u1);
		
		Set<UserRoleDto> nonAdminGlobalRoles = new HashSet<UserRoleDto>();
		UserRoleDto pduser = new UserRoleDto();
		pduser.setAlternatekey("pduser");
		nonAdminGlobalRoles.add(pduser);
		UserDto u2 = new UserDto();
		u2.setUserToken(nonadminUsername);
		u2.setGlobalRoles(nonAdminGlobalRoles);
		UserDto u3 = new UserDto();
		u3.setUserToken(nonadminUsername2);
		u3.setGlobalRoles(nonAdminGlobalRoles);
		companyUsers.add(u2);
		companyUsers.add(u3);

		when(userRepository.getOneUser(adminUsername)).thenReturn(u1);
		when(userRepository.getOneUser(nonadminUsername)).thenReturn(u2);
		when(userRepository.getOneUser(nonadminUsername2)).thenReturn(u3);

		int nonAdminUsersCount = service.getNonAdminUsersCount(companyUsers);
		
		assertThat(nonAdminUsersCount).isEqualTo(2);

	}
}
