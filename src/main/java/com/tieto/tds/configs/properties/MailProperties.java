package com.tieto.tds.configs.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("mail")
public class MailProperties {

	private String host;
	
	private Report report;

	public static class Report {

		private String[] receivers;

		public String[] getReceivers() {
			return receivers;
		}

		public void setReceivers(String[] receivers) {
			this.receivers = receivers;
		}
	}
	
	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public Report getReport() {
		return report;
	}

	public void setReport(Report report) {
		this.report = report;
	}
}
