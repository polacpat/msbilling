package com.tieto.tds.dtos.company.project.aur;

public class AurApp {

	private String appid;
	private String name;
	private String fqdn;
	private AurBillingDto billing;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFqdn() {
		return fqdn;
	}
	public void setFqdn(String fqdn) {
		this.fqdn = fqdn;
	}
	public AurBillingDto getBilling() {
		if (billing != null){
			return billing;
		}
		return new AurBillingDto();
	}
	public void setBilling(AurBillingDto billing) {
		this.billing = billing;
	}
	public String getAppid() {
		return appid;
	}
	public void setAppid(String appid) {
		this.appid = appid;
	}

}
