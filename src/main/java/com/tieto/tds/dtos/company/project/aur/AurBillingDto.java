package com.tieto.tds.dtos.company.project.aur;

public class AurBillingDto {

	private SaasFeeType type;
	private Double value;
	private Double userSuppoertFee;

	public SaasFeeType getType() {
		return type;
	}
	public void setType(SaasFeeType type) {
		this.type = type;
	}
	public Double getValue() {
		if (value == null){
			return 0d;
		}
		return value;
	}
	public void setValue(Double value) {
		this.value = value;
	}
	public Double getUserSuppoertFee() {
		if (userSuppoertFee == null){
			return 0d;
		}
		return userSuppoertFee;
	}
	public void setUserSuppoertFee(Double userSuppoertFee) {
		this.userSuppoertFee = userSuppoertFee;
	}
}
