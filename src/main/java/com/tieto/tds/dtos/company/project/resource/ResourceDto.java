package com.tieto.tds.dtos.company.project.resource;

public class ResourceDto {

	private long id;

	private String tenantid;
	
	private String name;

	private ResourceCapacity capacity;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTenantid() {
		return tenantid;
	}

	public void setTenantid(String tenantid) {
		this.tenantid = tenantid;
	}

	public ResourceCapacity getCapacity() {
		return capacity;
	}

	public void setCapacity(ResourceCapacity capacity) {
		this.capacity = capacity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
