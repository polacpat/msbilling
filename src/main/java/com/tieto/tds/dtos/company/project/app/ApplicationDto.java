package com.tieto.tds.dtos.company.project.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.tieto.tds.dtos.company.project.server.ServerDto;
import com.tieto.tds.dtos.user.UserDto;

public class ApplicationDto {

	private String appid;
	private String status;
	private String auroraStatus;

	private boolean shared;

	private StoreItemDto storeItem;
	private ServerDto server;
	
	private List<ConnectionDto> connections;

	private Date dateCreated;
	private UserDto createdBy;
	private Date dateDeleted;
	private UserDto deletedBy;
	private double costPrice;
	private String costPeriod;

	public StoreItemDto getStoreItem() {
		if (storeItem == null){
			return new StoreItemDto();
		}
		return storeItem;
	}

	public void setStoreItem(StoreItemDto storeItem) {
		this.storeItem = storeItem;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public ServerDto getServer() {
		if (server == null){
			return new ServerDto();
		}
		return server;
	}

	public void setServer(ServerDto server) {
		this.server = server;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public List<ConnectionDto> getConnections() {
		if (connections == null){
			return new ArrayList<>();
		}
		return connections;
	}

	public void setConnections(List<ConnectionDto> connections) {
		this.connections = connections;
	}

	public boolean isShared() {
		return shared;
	}

	public void setShared(boolean shared) {
		this.shared = shared;
	}

	public String getAuroraStatus() {
		return auroraStatus;
	}

	public void setAuroraStatus(String auroraStatus) {
		this.auroraStatus = auroraStatus;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public UserDto getCreatedBy() {
		if (createdBy == null){
			return new UserDto();
		}
		return createdBy;
	}

	public void setCreatedBy(UserDto createdBy) {
		this.createdBy = createdBy;
	}

	public String getCostPeriod() {
		return costPeriod;
	}

	public double getCostPrice() {
		return costPrice;
	}

	public void setCostPrice(double costPrice) {
		this.costPrice = costPrice;
	}

	public void setCostPeriod(String costPeriod) {
		this.costPeriod = costPeriod;
	}

	public Date getDateDeleted() {
		return dateDeleted;
	}

	public void setDateDeleted(Date dateDeleted) {
		this.dateDeleted = dateDeleted;
	}

	public UserDto getDeletedBy() {
		if (deletedBy == null){
			return new UserDto();
		}
		return deletedBy;
	}

	public void setDeletedBy(UserDto deletedBy) {
		this.deletedBy = deletedBy;
	}

	@Override
	public String toString() {
		String title = getStoreItem().getTitle();
		return String.format("ID: %s, title: %s", getAppid(), title);
	}
}
