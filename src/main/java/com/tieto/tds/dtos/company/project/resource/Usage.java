package com.tieto.tds.dtos.company.project.resource;

import java.time.LocalDateTime;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.tieto.tds.configs.Constants;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Usage implements Comparable<Usage> {

	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern=Constants.BACKEND_TIME_FORMAT, timezone="CET")
	private LocalDateTime timestamp; 
	private long vcpu;
	private long vram;
	private long vdisk;
	private long instances;
	private long images;
	@JsonProperty("volume_snapshots")
	private long volumeSnapshots;
	private long volumes;

	@JsonIgnore
	private boolean processed;

	public Usage() {
		super();
	}

	public Usage(LocalDateTime  timestamp, long instances, long vcpu, long vram, long vdisk, long images, long volumeSnapshots, long volumes) {
		super();
		this.timestamp = timestamp;
		this.instances = instances;
		this.vcpu = vcpu;
		this.vram = vram;
		this.vdisk = vdisk;
		this.images = images;
		this.volumeSnapshots = volumeSnapshots;
		this.volumes = volumes;
	}

	public long getVcpu() {
		return vcpu;
	}
	public void setVcpu(long vcpu) {
		this.vcpu = vcpu;
	}
	public long getVram() {
		return vram;
	}
	public void setVram(long vram) {
		this.vram = vram;
	}
	public long getVdisk() {
		return vdisk;
	}
	public void setVdisk(long vdisk) {
		this.vdisk = vdisk;
	}

	@Override
	public String toString() {
		return timestamp+" (cpu:"+vcpu+" ram:"+vram+" disk:"+vdisk+" instances:"+instances
				+" images:"+images+" volume_snapshots:"+volumeSnapshots+" volumes:"+volumes+")";
	}

	@Override
	public int compareTo(Usage otherInstance) {
		return this.timestamp.compareTo(otherInstance.getTimestamp());
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	public long getInstances() {
		return instances;
	}

	public void setInstances(long instances) {
		this.instances = instances;
	}

	public long getImages() {
		return images;
	}

	public void setImages(long images) {
		this.images = images;
	}

	public long getVolumeSnapshots() {
		return volumeSnapshots;
	}

	public void setVolumeSnapshots(long volumeSnapshots) {
		this.volumeSnapshots = volumeSnapshots;
	}

	public long getVolumes() {
		return volumes;
	}

	public void setVolumes(long volumes) {
		this.volumes = volumes;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public boolean equals(Object obj) {
		// self check
		if (this == obj)
			return true;
		// null check
		if (obj == null)
			return false;
		// type check and cast
		if (getClass() != obj.getClass())
			return false;
		Usage usage = (Usage) obj;
		// field comparison
		return Objects.equals(timestamp, usage.timestamp)
				&& Objects.equals(instances, usage.instances)
				&& Objects.equals(vcpu, usage.vcpu)
				&& Objects.equals(vram, usage.vram)
				&& Objects.equals(vdisk, usage.vdisk)
				&& Objects.equals(images, usage.images)
				&& Objects.equals(volumeSnapshots, usage.volumeSnapshots)
				&& Objects.equals(volumes, usage.volumes);
	}
	
	@Override
	public int hashCode() {
		return getTimestamp().hashCode() + (int)getInstances() + (int)getVcpu() + (int)getVram() +
				(int)getVdisk()+ (int)getImages() + (int)getVolumeSnapshots() + (int)getVolumeSnapshots();
	}
}
