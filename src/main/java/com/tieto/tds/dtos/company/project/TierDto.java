package com.tieto.tds.dtos.company.project;

import java.util.Calendar;

public class TierDto {

	private long id;

	private boolean defaultTier;

	private String title;

	private FeeDto currentFee;

	public TierDto() {
		super();
	}

	public TierDto(long id, boolean defaultTier, String title, FeeDto currentFee) {
		super();
		this.id = id;
		this.defaultTier = defaultTier;
		this.title = title;
		this.currentFee = currentFee;
	}

	public boolean isDefaultTier() {
		return defaultTier;
	}

	public void setDefaultTier(boolean defaultTier) {
		this.defaultTier = defaultTier;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public FeeDto getCurrentFee() {
		if (currentFee != null){
			return currentFee;
		} else {
			return new FeeDto(1, Calendar.getInstance().getTime(), 0d, 0d, 0d, 0d, 0d, 0d);
		}
	}

	public void setCurrentFee(FeeDto currentFee) {
		this.currentFee = currentFee;
	}

}
