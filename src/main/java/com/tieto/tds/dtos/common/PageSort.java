package com.tieto.tds.dtos.common;

public enum PageSort {

	ASC, DESC
}
