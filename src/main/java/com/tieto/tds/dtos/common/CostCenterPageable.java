package com.tieto.tds.dtos.common;

import java.util.List;

import com.tieto.tds.dtos.company.CostCenterDto;

public class CostCenterPageable extends Pageable{

	private List<CostCenterDto> content;

	public List<CostCenterDto> getContent() {
		return content;
	}

	public void setContent(List<CostCenterDto> content) {
		this.content = content;
	}
}
