package com.tieto.tds.dtos.invoicing;

public class CompanyInvoiceDto extends InvoiceDto {

	private String companyid;

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
	
}
