package com.tieto.tds.dtos.invoicing;

public enum InstanceType {

	ORCHESTRATED, EXTERNAL;

}
