package com.tieto.tds.dtos.invoicing.pages;

import java.util.List;

import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;

public class ProjectInvoicePageDto extends PageDto{

	private List<ProjectInvoiceDto> content;

	public List<ProjectInvoiceDto> getContent() {
		return content;
	}

	public void setContent(List<ProjectInvoiceDto> content) {
		this.content = content;
	}

}
