package com.tieto.tds.dtos.invoicing.pages;

import java.util.List;

import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;

public class CostCenterInvoicePageDto extends PageDto{

	private List<CostCenterInvoiceDto> content;

	public List<CostCenterInvoiceDto> getContent() {
		return content;
	}

	public void setContent(List<CostCenterInvoiceDto> content) {
		this.content = content;
	}
}
