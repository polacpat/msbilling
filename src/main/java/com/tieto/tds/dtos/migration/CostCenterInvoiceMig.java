package com.tieto.tds.dtos.migration;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CostCenterInvoiceMig extends InvoiceMig{

	@JsonProperty("costcenter_id")
	private String costcenterid;

	public String getCostcenterid() {
		return costcenterid;
	}

	public void setCostcenterid(String costcenterid) {
		this.costcenterid = costcenterid;
	}

}
