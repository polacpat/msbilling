package com.tieto.tds.events;

import org.springframework.context.ApplicationEvent;

public class GenerateReportEvent extends ApplicationEvent {

	private static final long serialVersionUID = 3216127082248612787L;

	private String period;

	public GenerateReportEvent(String source) {
		super(source);
		this.period = source;
	}

	public String getPeriod() {
		return period;
	}
	
}
