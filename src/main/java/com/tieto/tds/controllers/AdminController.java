package com.tieto.tds.controllers;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tieto.tds.repositories.BackendRepository;
import com.tieto.tds.services.AdminService;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.MigrationService;
import com.tieto.tds.services.commons.LocalDateHolder;
import com.tieto.tds.services.reports.ExportInvoiceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * Rest controller for handling admin tasks
 */
@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
@Api(tags={"Admin"})
public class AdminController {

	private static final Logger log = LoggerFactory.getLogger(AdminController.class);

	private AdminService adminService;

	private ExportInvoiceService exportInvoiceService;
	
	private MigrationService migrationService;

	private BackendRepository backendRepository;

	private LocalDateUtilService localDateUtilService;

	@Autowired
	public AdminController(AdminService adminService,
			ExportInvoiceService exportInvoiceService,
			MigrationService migrationService,
			BackendRepository backendRepository,
			LocalDateUtilService localDateUtilService) {
		super();
		this.adminService = adminService;
		this.exportInvoiceService = exportInvoiceService;
		this.migrationService = migrationService;
		this.backendRepository = backendRepository;
		this.localDateUtilService = localDateUtilService;
	}

	@ApiOperation(value = "Export custom jenkins for invoicing")
	@RequestMapping(path="/admin/customjenkins", method = RequestMethod.GET)
	public void configureUsers(){
		adminService.getCustomJenkinsForInvoicing();
	}

	/**
	 * generate invoices for whole environment for selected time period
	 * @param periodFrom specify starting day of invoice period
	 * @param periodTo specify ending day of invoice period
	 */
	@ApiOperation(value = "Export custom jenkins for invoicing")
	@RequestMapping(path="/admin/generateinvoices", method = RequestMethod.POST)
	public void generateInvoice(
			@RequestParam(value="from", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodFrom,
			@RequestParam(value="to", required=false) @DateTimeFormat(pattern="yyyy-MM-dd") LocalDate periodTo){
		log.info("generate invoices for periodFrom = {}, periodTo = {}", periodFrom, periodTo);

		LocalDateHolder localDateHolder = localDateUtilService.getFromAndToFromInput(periodFrom, periodTo);

		adminService.generateInvoices(localDateHolder.getFrom(), localDateHolder.getTo());
	}

	/**
	 * @param number describes time period for required time period in format yyyy-MM
	 * @return excel sheet
	 */
	@ApiOperation(value = "Export invoices for selected period")
	@RequestMapping(path="/admin/exportinvoices", method = RequestMethod.GET)
	public byte[] exportInvoices(@RequestParam(value="number", required=true) String number) {
		return exportInvoiceService.exportInvoicesByPeriod(number).toByteArray();
	}

	/**
	 * migrate invoices from old mediator DB stored in JSON files on disk into new billing database
	 */
	@ApiOperation(value = "Export custom jenkins for invoicing")
	@RequestMapping(path="/admin/migrateinvoices", method = RequestMethod.GET)
	public void migrateInvoices() {
		migrationService.migrateInvoices();
	}

	/**
	 * helper call to re-trigger backend login
	 */
	@ApiOperation(value = "Relogin to backend")
	@RequestMapping(path="/admin/login", method = RequestMethod.GET)
	public void loginToBackend() {
		backendRepository.doLogin();
	}

}
