package com.tieto.tds.domains.invoice;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name = "b_company_invoice")
@DiscriminatorValue("company")
public class CompanyInvoice extends Invoice {

	private static final long serialVersionUID = -1569982680734172764L;

	@Column
	private String companyid;

	public CompanyInvoice(){
		super();
	}

	public CompanyInvoice(String companyid, String invoiceNumber) {
		super();
		super.setNumber(invoiceNumber);
		this.companyid = companyid;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	@Override
	public String toString() {
		return super.toString() + ", companyid: " + companyid;
	}
}
