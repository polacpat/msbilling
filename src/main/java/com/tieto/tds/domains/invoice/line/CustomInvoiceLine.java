package com.tieto.tds.domains.invoice.line;


import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.tieto.tds.domains.common.DbEntity;

@Entity(name = "b_custom_invoice_line")
public class CustomInvoiceLine extends DbEntity {

	private static final long serialVersionUID = 8881453331014686674L;

	@Column
	private String projectid;

	@Column
	private String companyid;

	@Column
	private String costCenterId;

	@Column
	private String itemName;

	@Column
	private String unit;

	@Column
	private Double amount;

	@Column
	private BigDecimal price;

	@Column
	private String no;

	@Column
	private Date dateCreated = Calendar.getInstance().getTime();

	@Column
	private Date lastUpdated;

	public CustomInvoiceLine() {
		super();
	}

	public CustomInvoiceLine(String projectid, Date dateCreated, BigDecimal price) {
		super();
		this.projectid = projectid;
		this.dateCreated = dateCreated;
		this.price = price;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getProjectid() {
		return projectid;
	}

	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}

	public String getNo() {
		return no;
	}

	public void setNo(String no) {
		this.no = no;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getCostCenterId() {
		return costCenterId;
	}

	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}

}