package com.tieto.tds.domains.invoice;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity(name="b_costcenter_invoice")
@DiscriminatorValue("costcenter")
public class CostCenterInvoice extends Invoice {

	private static final long serialVersionUID = 3042722613686678167L;

	@Column
	private String companyid;

	@Column(name="costcenterid")
	private String costCenterId;

	public CostCenterInvoice(){
		super();
	}

	public CostCenterInvoice(String companyid, String costCenterId, String invoiceNumber) {
		super(invoiceNumber);
		this.companyid = companyid;
		this.costCenterId = costCenterId;
	}

	public String getCompanyid() {
		return companyid;
	}

	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}

	public String getCostCenterId() {
		return costCenterId;
	}

	public void setCostCenterId(String costCenterId) {
		this.costCenterId = costCenterId;
	}

	@Override
	public String toString() {
		return super.toString() + ", costCenter: " + costCenterId;
	}
}
