package com.tieto.tds.domains.invoice;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;

import com.tieto.tds.domains.common.DbEntity;
import com.tieto.tds.domains.invoice.line.InvoiceLine;


@Entity(name="b_invoice")
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
@DiscriminatorColumn(name="invoicetype")
public abstract class Invoice extends DbEntity {

	private static final long serialVersionUID = -1626349867128229453L;

	@Column
	private String number;

	@OneToMany(mappedBy = "invoice", cascade=CascadeType.REMOVE, fetch=FetchType.EAGER)
	private List<InvoiceLine> lines;

	@Column(name="date_created")
	private Date dateCreated = new Date();

	public Invoice(){
		super();
	}

	public Invoice(String number){
		super();
		this.number = number;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	@Override
	public String toString() {
		return "ID:" + getId() + ", no: " + number;
	}

	public List<InvoiceLine> getLines() {
		return lines;
	}

	public void setLines(List<InvoiceLine> lines) {
		this.lines = lines;
	}
	
	public void addLines(InvoiceLine line) {
		this.lines.add(line);
	}
}
