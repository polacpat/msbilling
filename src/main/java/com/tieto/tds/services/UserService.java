package com.tieto.tds.services;

import java.util.List;

import com.tieto.tds.dtos.user.UserDto;

public interface UserService {

	public int getNonAdminUsersCount(List<UserDto> companyUsers);
}
