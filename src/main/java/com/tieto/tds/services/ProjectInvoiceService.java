package com.tieto.tds.services;

import java.time.LocalDate;

import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;

public interface ProjectInvoiceService {

	public ProjectInvoiceDto createProjectInvoice(String companyid, String projectid, LocalDate periodFrom,  LocalDate periodTo);

	public ProjectInvoiceDto findOne(String companyid, String projectid, long id);

	public ProjectInvoicePageDto findAllByProjectid(String companyid, String projectid, PageableMs pageable);

	public ProjectInvoicePageDto findAllByCompanyid(String companyid, String no, PageableMs pageable);
}
