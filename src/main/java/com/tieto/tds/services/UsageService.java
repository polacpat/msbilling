package com.tieto.tds.services;

import java.util.List;

import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.services.commons.ResourceUsage;

public interface UsageService {

	public ResourceUsage getTotalUsage(List<Usage> tenantUsage);
}
