package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.AurUserDto;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.AurRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.services.CostCenterHelper;
import com.tieto.tds.services.CostCenterInvoiceLineService;
import com.tieto.tds.services.UserService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CostCenterInvoiceLineServiceImpl implements CostCenterInvoiceLineService {

	private static final Logger log = LoggerFactory.getLogger(CostCenterInvoiceLineServiceImpl.class);

	private AurRepository aurRepository;

	private UserRepository userRepository;

	private CompanyRepository companyRepository;

	private UserService userService;

	private CostCenterHelper costCenterHelper;
	@Autowired
	public CostCenterInvoiceLineServiceImpl(AurRepository aurRepository, UserRepository userRepository,
			CompanyRepository companyRepository, UserService userService,
			CostCenterHelper costCenterHelper) {
		super();
		this.aurRepository = aurRepository;
		this.userRepository = userRepository;
		this.companyRepository = companyRepository;
		this.userService = userService;
		this.costCenterHelper = costCenterHelper;
	}

	@Override
	public List<InvoiceLineDto> getSaaSInvoiceLines(String companyid, String cctitle, String number) {
		log.debug("getSaaSInvoiceLines {}, {}, {}", companyid, cctitle, number);

		List<InvoiceLineDto> result = new ArrayList<>();
		List<AurApp> aurApps = aurRepository.findAllAurAppsByCompanyid(companyid);
		for (AurApp aurApp : aurApps) {
			log.debug("iterate over app {}", aurApp.getFqdn());
			if (costCenterHelper.isAplicableForCCInvoice(aurApp)){
				List<UserDto> aurUsers = aurRepository.findAllUsersByCostcenterAndAppid(companyid, cctitle, aurApp.getAppid());
				double amount = aurUsers.size();
				log.debug("There are {} users in CostCenter {} in app {}.", aurUsers.size(), cctitle, aurApp.getFqdn());

				String itemName = String.format("SaaS user fee for %s", aurApp.getFqdn());
				result.add( new InvoiceLineDto(itemName, "pcs", amount,
						new BigDecimal( aurApp.getBilling().getValue() )) );
			} else {
				log.debug("App {} is not applicable to CC invoice.", aurApp.getFqdn());
			}
		}

		return result;
	}

	@Override
	public List<InvoiceLineDto> getSaaSUserFeeInvoiceLines(String companyid, String cctitle) {
		log.debug("getSaaSUserFeeInvoiceLines ");
		List<InvoiceLineDto> result = new ArrayList<>();
		CompanyDto company = companyRepository.findOneCompany(companyid);
		if ( "per-costcenter".equals( company.getFeeType() )){
			List<UserDto> companyUsers = userRepository.findAllUsersByCompanyidAndCostCenterTitle(companyid, cctitle);
			log.debug("company {} cost center {} hase all together {} users.", company.getTitle(), cctitle, companyUsers.size());
			int nonAdminUsersCount = userService.getNonAdminUsersCount(companyUsers);
			log.debug("company {} cost center {} hase only {} non-tdsadmin users.", company.getTitle(), cctitle, nonAdminUsersCount);

			result.add( new InvoiceLineDto(Constants.COSTCENTER_USER_FEE, "pcs", new BigDecimal(nonAdminUsersCount), company.getUserFee()) );
		}

		return result;
	}

}
