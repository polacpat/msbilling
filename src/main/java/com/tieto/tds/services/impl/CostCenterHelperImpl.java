package com.tieto.tds.services.impl;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;
import com.tieto.tds.services.CostCenterHelper;

@Component
@Profile(Constants.PROFILE_DEFAULT)
public class CostCenterHelperImpl implements CostCenterHelper{

	@Override
	public boolean isAplicableForCCInvoice(AurApp aurApp) {
		if (aurApp.getBilling() != null && aurApp.getBilling().getType()==SaasFeeType.PER_USER)return true;
		return false;
	}

}
