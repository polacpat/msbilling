package com.tieto.tds.services.impl;

import java.io.File;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.Constants;
import com.tieto.tds.configs.properties.BillingProperties;
import com.tieto.tds.domains.invoice.CompanyInvoice;
import com.tieto.tds.domains.invoice.CostCenterInvoice;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.migration.CostCenterInvoiceMig;
import com.tieto.tds.dtos.migration.InvoiceLineMig;
import com.tieto.tds.dtos.migration.InvoiceMig;
import com.tieto.tds.dtos.migration.ProjectInvoiceMig;
import com.tieto.tds.repositories.invoicing.CompanyInvoiceRepository;
import com.tieto.tds.repositories.invoicing.CostCenterInvoiceRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.repositories.invoicing.ProjectInvoiceRepository;
import com.tieto.tds.services.MigrationService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class MigrationServiceImpl implements MigrationService{

	private static final Logger log = LoggerFactory.getLogger(MigrationServiceImpl.class);

	private static final String LOG_ID_INFO = "ID = {}";

	private BillingProperties billingProperties;

	private CompanyInvoiceRepository companyInvoiceRepository;

	private CostCenterInvoiceRepository costCenterInvoiceRepository;

	private ProjectInvoiceRepository projectInvoiceRepository;

	private InvoiceLineRepository invoiceLineRepository;

	@Autowired
	public MigrationServiceImpl(
			BillingProperties billingProperties,
			CompanyInvoiceRepository companyInvoiceRepository,
			CostCenterInvoiceRepository costCenterInvoiceRepository, ProjectInvoiceRepository projectInvoiceRepository,
			InvoiceLineRepository invoiceLineRepository) {
		super();
		this.billingProperties = billingProperties;
		this.companyInvoiceRepository = companyInvoiceRepository;
		this.costCenterInvoiceRepository = costCenterInvoiceRepository;
		this.projectInvoiceRepository = projectInvoiceRepository;
		this.invoiceLineRepository = invoiceLineRepository;
	}

	private ObjectMapper mapper = new ObjectMapper();
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");

	@Override
	public void migrateInvoices() {
		migrateProjectInvoices();

		migrateCompanyInvoices();

		migrateCostCenterInvoices();
		log.debug("migration finished");
	}

	private void migrateCostCenterInvoices() {
		log.debug("Migrate cost center invoices");
		try {
			CostCenterInvoiceMig[] companyInvoices = mapper.readValue(
					new File(billingProperties.getReportfolder(),"costcenterInvoice.json"), CostCenterInvoiceMig[].class);
			InvoiceLineMig[] companyInvoiceLines = mapper.readValue(
					new File(billingProperties.getReportfolder(),"costcenterInvoiceLines.json"), InvoiceLineMig[].class);

			for (CostCenterInvoiceMig invoiceMig : companyInvoices) {
				log.debug(LOG_ID_INFO, invoiceMig.getId());
				CostCenterInvoice invoice = new CostCenterInvoice();
				invoice.setCompanyid(invoiceMig.getCompanyid());
				invoice.setCostCenterId(invoiceMig.getCostcenterid());
				String no = sdf.format( invoiceMig.getPeriod() );
				invoice.setNumber( no );
				invoice.setDateCreated(invoiceMig.getDateCreated());
				costCenterInvoiceRepository.save(invoice);
				for (InvoiceLineMig lineMig : companyInvoiceLines) {
					if ( lineMig.getInvoice_id()==invoiceMig.getId() ) {
						InvoiceLine invoiceLine = new InvoiceLine();
						invoiceLine.setAmount( lineMig.getAmount() );
						invoiceLine.setDateCreated( lineMig.getDateCreated() );
						invoiceLine.setInvoice(invoice);
						invoiceLine.setItemName( lineMig.getItemName() );
						invoiceLine.setPrice( BigDecimal.valueOf(lineMig.getPrice()) );
						invoiceLine.setUnit( lineMig.getUnit());
						invoiceLineRepository.save(invoiceLine);
					}
				}
			}
		} catch (Exception e){
			log.error("cannot migrate cost center invoices. ", e);
		}
	}

	private void migrateCompanyInvoices() {
		log.debug("Migrate company invoices");
		try {
			InvoiceMig[] companyInvoices = mapper.readValue(
					new File(billingProperties.getReportfolder(), "companyInvoices.json"), InvoiceMig[].class);
			InvoiceLineMig[] companyInvoiceLines = mapper.readValue(
					new File(billingProperties.getReportfolder(), "companyInvoiceLines.json"), InvoiceLineMig[].class);

			for (InvoiceMig invoiceMig : companyInvoices) {
				log.debug(LOG_ID_INFO, invoiceMig.getId());
				CompanyInvoice invoice = new CompanyInvoice();
				invoice.setCompanyid(invoiceMig.getCompanyid());
				String no = sdf.format( invoiceMig.getPeriod() );
				invoice.setNumber( no );
				invoice.setDateCreated(invoiceMig.getDateCreated());
				companyInvoiceRepository.save(invoice);
				for (InvoiceLineMig lineMig : companyInvoiceLines) {
					if ( lineMig.getInvoice_id()==invoiceMig.getId() ) {
						InvoiceLine invoiceLine = new InvoiceLine();
						invoiceLine.setAmount( lineMig.getAmount() );
						invoiceLine.setDateCreated( lineMig.getDateCreated() );
						invoiceLine.setInvoice(invoice);
						invoiceLine.setItemName( lineMig.getItemName() );
						invoiceLine.setPrice( BigDecimal.valueOf(lineMig.getPrice()) );
						invoiceLine.setUnit( lineMig.getUnit());
						invoiceLineRepository.save(invoiceLine);
					}
				}
			}
		} catch (Exception e) {
			log.error("cannot migrate company invoices. ", e);
		}
	}

	private void migrateProjectInvoices() {
		log.debug("Migrate project invoices");
		try {
			ProjectInvoiceMig[] projectInvoices = mapper.readValue(
					new File(billingProperties.getReportfolder(), "projectInvoices.json"), ProjectInvoiceMig[].class);
			InvoiceLineMig[] projectInvoiceLines = mapper.readValue(
					new File(billingProperties.getReportfolder(), "projectInvoiceLines.json"), InvoiceLineMig[].class);

			for (ProjectInvoiceMig invoiceMig : projectInvoices) {
				log.debug(LOG_ID_INFO, invoiceMig.getId());
				ProjectInvoice invoice = new ProjectInvoice();
				invoice.setCompanyid(invoiceMig.getCompanyid());
				invoice.setProjectid(invoiceMig.getProductid());
				String no = sdf.format( invoiceMig.getPeriod() );
				invoice.setNumber( no );
				invoice.setDateCreated(invoiceMig.getDateCreated());
				projectInvoiceRepository.save(invoice);
				for (InvoiceLineMig lineMig : projectInvoiceLines) {
					if ( lineMig.getInvoice_id()==invoiceMig.getId() ) {
						InvoiceLine invoiceLine = new InvoiceLine();
						invoiceLine.setAmount( lineMig.getAmount() );
						invoiceLine.setDateCreated( lineMig.getDateCreated() );
						invoiceLine.setInvoice(invoice);
						invoiceLine.setItemName( lineMig.getItemName() );
						invoiceLine.setPrice( BigDecimal.valueOf(lineMig.getPrice()) );
						invoiceLine.setUnit( lineMig.getUnit());
						invoiceLineRepository.save(invoiceLine);
					}
				}
			}
		} catch (Exception e) {
			log.error("cannot migrate project invoices. ", e);
		}
	}

}
