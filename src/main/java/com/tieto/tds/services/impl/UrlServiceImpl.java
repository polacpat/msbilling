package com.tieto.tds.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.services.UrlService;

@Service
public class UrlServiceImpl implements UrlService{

	private PortalProperties properties;
	
	@Autowired
	public UrlServiceImpl(PortalProperties properties) {
		super();
		this.properties = properties;
	}

	@Override
	public String getProjectUrl(ProjectDto project) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("{context}/#/areas/{areaid}/projects/{projectid}/billing").buildAndExpand(
						properties.getContext(), project.getCompany().getAreaid(), project.getProductId());
		return uriComponents.toUriString();
	}

	@Override
	public String getCostCenterUrl(CostCenterDto costCenter) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("{context}/#/areas/{areaid}/ccinvoices/{ccid}/billing").buildAndExpand(
						properties.getContext(), costCenter.getCompany().getAreaid(), costCenter.getId());
		return uriComponents.toUriString();
	}

	@Override
	public String getCompanyUrl(CompanyDto company) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("{context}/#/areas/{areaid}/billing").buildAndExpand(
						properties.getContext(), company.getAreaid());
		return uriComponents.toUriString();
	}

}
