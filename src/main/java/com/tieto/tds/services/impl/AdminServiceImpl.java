package com.tieto.tds.services.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.ProjectInvoice;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.events.GenerateReportEvent;
import com.tieto.tds.repositories.BackendRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.CostCenterRepository;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.invoicing.ProjectInvoiceRepository;
import com.tieto.tds.services.AdminService;
import com.tieto.tds.services.CompanyInvoiceService;
import com.tieto.tds.services.CostCenterInvoiceService;
import com.tieto.tds.services.ProjectInvoiceService;

/**
 * Service for handling admin tasks
 */
@Service
@Profile(Constants.PROFILE_DEFAULT)
public class AdminServiceImpl implements AdminService{

	private static final Logger log = LoggerFactory.getLogger(AdminServiceImpl.class);

	private ProjectInvoiceRepository projectInvoiceRepository;

	private ProjectRepository projectRepository;

	private CompanyRepository companyRepository;

	private CostCenterRepository costCenterRepository;

	private ProjectInvoiceService projectInvoiceService;

	private CostCenterInvoiceService costCenterInvoiceService;

	private CompanyInvoiceService companyInvoiceService;

	private BackendRepository backendRepository;

	private ApplicationEventPublisher eventPublisher;

	@Autowired
	public AdminServiceImpl(
			ProjectInvoiceRepository invoiceRepository,
			ProjectRepository projectRepository,
			CompanyRepository companyRepository,
			ProjectInvoiceService projectInvoiceService,
			CostCenterInvoiceService costCenterInvoiceService,
			CostCenterRepository costCenterRepository,
			CompanyInvoiceService companyInvoiceService,
			BackendRepository backendRepository,
			ApplicationEventPublisher eventPublisher) {
		super();
		this.projectInvoiceRepository = invoiceRepository;
		this.projectRepository = projectRepository;
		this.companyRepository = companyRepository;
		this.projectInvoiceService = projectInvoiceService;
		this.costCenterInvoiceService = costCenterInvoiceService;
		this.costCenterRepository = costCenterRepository;
		this.companyInvoiceService = companyInvoiceService;
		this.backendRepository = backendRepository;
		this.eventPublisher = eventPublisher;
	}

	@Override
	public void getCustomJenkinsForInvoicing(){
		List<ProjectInvoice> invoices = projectInvoiceRepository.findAll();
		for (ProjectInvoice invoice : invoices) {
			log.debug("ProjectInvoice " + invoice.getId() );
		}
	}

	@Override
	public void generateInvoices(LocalDate periodFrom, LocalDate periodTo) {
		log.debug("generateInvoices from {} to {}.", periodFrom, periodTo);

		backendRepository.doLogin();

		for (CompanyDto companyDto : companyRepository.findAll()) {
			generateProjectInvoices(periodFrom, periodTo, companyDto);
			generateCostCenterinvoices(periodFrom, periodTo, companyDto);
			generateCompanyInvoice(periodFrom, periodTo, companyDto);
		}

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String period = periodFrom.format(formatter);

		eventPublisher.publishEvent(new GenerateReportEvent(period));

	}

	private void generateCompanyInvoice(LocalDate periodFrom, LocalDate periodTo, CompanyDto companyDto) {
		log.debug("GENERATE COMPANY INVOICE {}, {}", companyDto.getTitle(), companyDto.getAreaid());
		try {
			companyInvoiceService.createInvoice(
					companyDto.getAreaid(),
					periodFrom,
					periodTo);
		} catch (Exception e){
			log.error("Error generating company invoice {}, {}", companyDto.getTitle(), companyDto.getAreaid(), e);
		}
		log.debug("COMPANY INVOICE GENERATED {}, {}", companyDto.getTitle(), companyDto.getAreaid());
	}

	private void generateCostCenterinvoices(LocalDate periodFrom, LocalDate periodTo, CompanyDto companyDto) {
		log.debug("GENERATE CC INVOICE FOR COMPANY {}, {}", companyDto.getTitle(), companyDto.getAreaid());
		for (CostCenterDto costCenterDto : costCenterRepository.findAllCostCentersByCompany(companyDto.getAreaid())) {
			try {
				costCenterInvoiceService.createCostCenterInvoiceByDate(
						companyDto.getAreaid(),
						costCenterDto.getTitle(),
						periodFrom,
						periodTo);
			} catch (Exception e){
				log.error("Error generating CC invoice for company {}, {}", companyDto.getTitle(), companyDto.getAreaid(), e);
			}
		}
		log.debug("CC INVOICE GENERATER FOR COMPANY {}, {}", companyDto.getTitle(), companyDto.getAreaid());
	}

	private void generateProjectInvoices(LocalDate periodFrom, LocalDate periodTo, CompanyDto companyDto) {
		log.debug("GENERATE PROJECT INVOICE FOR COMPANY {}, {}", companyDto.getTitle(), companyDto.getAreaid());
		for (ProjectDto projectDto : projectRepository.findAllProjectsByCompany(companyDto.getAreaid())) {
			try {
				projectInvoiceService.createProjectInvoice(
						companyDto.getAreaid(),
						projectDto.getProductId(),
						periodFrom,
						periodTo);
			} catch (Exception e){
				log.error("Error generating project invoice for company {}, {}", companyDto.getTitle(), companyDto.getAreaid(), e);
			}
		}
		log.debug("PROJECT INVOICE GENERATED FOR COMPANY {}, {}", companyDto.getTitle(), companyDto.getAreaid());
	}

}
