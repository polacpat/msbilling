package com.tieto.tds.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.company.UserRoleDto;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.services.UserService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class UserServiceImpl implements UserService{

	private UserRepository userRepository;

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
	}

	@Override
	public int getNonAdminUsersCount(List<UserDto> companyUsers) {
		int nonAdminUsersCount = 0;
		for (UserDto user: companyUsers){
			boolean admin = false;
			UserDto userDto =  userRepository.getOneUser(user.getUserToken());
			for (UserRoleDto userRoleDto : userDto.getGlobalRoles()) {
				if ( "pdcadmin".equals( userRoleDto.getAlternatekey() )){
					admin = true;
				}
			}
			if (!admin)nonAdminUsersCount++;
		}
		return nonAdminUsersCount;
	}

}
