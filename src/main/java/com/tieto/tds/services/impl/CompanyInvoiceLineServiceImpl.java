package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.company.project.aur.SaasFeeType;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.AurRepository;
import com.tieto.tds.repositories.CompanyRepository;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.UserRepository;
import com.tieto.tds.repositories.invoicing.CompanyInvoiceRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.CompanyInvoiceLineService;
import com.tieto.tds.services.CostCenterInvoiceService;
import com.tieto.tds.services.InvoiceLineService;
import com.tieto.tds.services.ProjectInvoiceService;
import com.tieto.tds.services.UserService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CompanyInvoiceLineServiceImpl implements CompanyInvoiceLineService{

	private static final Logger log = LoggerFactory.getLogger(CompanyInvoiceLineServiceImpl.class);

	private ProjectInvoiceService projectInvoiceService;

	private CostCenterInvoiceService costCenterInvoiceService;

	private UserRepository userRepository;

	private CompanyRepository companyRepository;

	private ProjectRepository projectRepository;

	private AurRepository aurRepository;

	private UserService userService;

	private InvoiceLineService invoiceLineService;

	@Autowired
	public CompanyInvoiceLineServiceImpl(
			ProjectInvoiceService projectInvoiceService, UserRepository userRepository,
			CompanyRepository companyRepository, ProjectRepository projectRepository,
			AurRepository aurRepository, UserService userService,
			CostCenterInvoiceService costCenterInvoiceService,
			InvoiceLineService invoiceLineService) {
		super();
		this.projectInvoiceService = projectInvoiceService;
		this.costCenterInvoiceService = costCenterInvoiceService; 
		this.userRepository = userRepository;
		this.companyRepository = companyRepository;
		this.projectRepository = projectRepository;
		this.aurRepository = aurRepository;
		this.userService = userService;
		this.invoiceLineService = invoiceLineService;
	}

	@Override
	public List<InvoiceLineDto> getProjectsInvoiceLines(String companyid, String no) {
		log.info("getProjectsInvoiceLines {}, {}.", companyid, no);
		PageableMs pageable = new PageableMs(1, 1000, PageSort.ASC, "id");
		ProjectInvoicePageDto projectInvoices = projectInvoiceService.findAllByCompanyid(companyid, no, pageable);
		List<InvoiceLineDto> result = new ArrayList<>();

		for (ProjectInvoiceDto invoiceDto : projectInvoices.getContent()) {
			try {
				ProjectDto project = projectRepository.findOneProject(companyid, invoiceDto.getProjectid());
				BigDecimal totalPrice = invoiceLineService.getTotalPriceByInvoiceID(invoiceDto.getId());
				result.add( new InvoiceLineDto("Project " + project.getName(), "pcs", 1d, totalPrice) );
			} catch (Exception e){
				log.error("Canot get project for company {}, {}", companyid, invoiceDto.getProjectid(), e);
			}
		}
		return result;
	}

	@Override
	public List<InvoiceLineDto> getCostCenterInvoiceLines(String companyid, String invoiceNumber) {
		log.info("getCostCenterInvoiceLines {}, {}.", companyid, invoiceNumber);
		PageableMs pageable = new PageableMs(1, 1000, PageSort.ASC, "id");
		CostCenterInvoicePageDto costCenterInvoices = costCenterInvoiceService.findAllByCompanyidAndNo(companyid, invoiceNumber, pageable);
		List<InvoiceLineDto> result = new ArrayList<>();

		for (CostCenterInvoiceDto costCenterInvoiceDto : costCenterInvoices.getContent()) {
			BigDecimal totalPrice = invoiceLineService.getTotalPriceByInvoiceID(costCenterInvoiceDto.getId());
			result.add( new InvoiceLineDto("Cost center " + costCenterInvoiceDto.getCostCenterId(), "pcs", 1d, totalPrice) );
		}

		return result;
	}

	/**
	 * get invoice line related to SaaS app which are invoices on company level
	 */
	@Override
	public List<InvoiceLineDto> getSaaSInvoiceLines(String companyid, String no, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("getSaaSInvoiceLines, {}, {}.", companyid, no);
		List<InvoiceLineDto> result = new ArrayList<>();
		List<AurApp> aurApps = aurRepository.findAllAurAppsByCompanyid(companyid);
		for (AurApp aurApp : aurApps) {
			if (aurApp.getBilling() != null && aurApp.getBilling().getType()==SaasFeeType.PER_SERVICE){
				String itemName = String.format("SaaS application fee for %s", aurApp.getFqdn());
				result.add( new InvoiceLineDto(itemName, "pcs", 1d, new BigDecimal( aurApp.getBilling().getValue() )) );

				result.add( getUserSupportFee(companyid, aurApp));
			}
		}
		return result;
	}

	private InvoiceLineDto getUserSupportFee(String companyid, AurApp aurApp) {
		log.debug("getUserSupportFee, {}, {}", companyid, aurApp.getAppid());
		CompanyDto company = companyRepository.findOneCompany(companyid);
		log.debug("Find all aur users in shared project {}, appid = {}.", company.getSharedProductid(), aurApp.getAppid());
		List<UserDto> allUsers = aurRepository.findAllUsersByProjectAndAppid(companyid, company.getSharedProductid(), aurApp.getAppid());
		double nonAdminUsersCount = userService.getNonAdminUsersCount(allUsers);

		Double userSuppoertFee = aurApp.getBilling().getUserSuppoertFee();

		String itemName = String.format("SaaS application user support fee for %s", aurApp.getFqdn());
		return new InvoiceLineDto(itemName, "pcs", nonAdminUsersCount, new BigDecimal( userSuppoertFee ));
	}

	@Override
	public List<InvoiceLineDto> getUserFeeInvoiceLines(String companyid, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("getUserFeeInvoiceLines, {}.", companyid);
		CompanyDto company = companyRepository.findOneCompany(companyid);
		List<InvoiceLineDto> result = new ArrayList<>();

		if ( "per-company".equals( company.getFeeType() )){
			List<UserDto> companyUsers = userRepository.findALlUsersByCompanyid(companyid);
			int nonAdminUsersCount = userService.getNonAdminUsersCount(companyUsers);

			result.add( new InvoiceLineDto("Company user fee", "pcs", new BigDecimal(nonAdminUsersCount), company.getUserFee()) );
		}

		result.add( new InvoiceLineDto("On call fee", "pcs", new BigDecimal(1), company.getOncallFee()) );
		return result;
	}

}
