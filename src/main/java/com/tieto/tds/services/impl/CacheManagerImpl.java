package com.tieto.tds.services.impl;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.services.CacheManager;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CacheManagerImpl implements CacheManager{

	@CacheEvict(value = "project", key = "#projectid")
	@Override
	public void onProjectid(String projectid){
		// Nothing to do
	}
	
	@CacheEvict(value = "company", key = "#compandyid")
	@Override
	public void onCompanyid(String compandyid){
		// Nothing to do
	}
}
