package com.tieto.tds.services.impl;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.tieto.tds.exceptions.PDCException;
import com.tieto.tds.services.LocalDateUtilService;
import com.tieto.tds.services.commons.LocalDateHolder;

@Service
public class LocalDateUtilServiceImpl implements LocalDateUtilService {

	private static final Logger log = LoggerFactory.getLogger(LocalDateUtilServiceImpl.class);

	@Override
	public LocalDateHolder getFromAndToFromInput(LocalDate periodFrom, LocalDate periodTo) {
		LocalDate periodFromOut;
		LocalDate periodToOut;
		log.info("periodFrom = {}, periodTo = ", periodFrom, periodTo);

		if (periodFrom == null) {
			periodFromOut = LocalDate.now().withDayOfMonth(1);
			periodToOut = periodFromOut.plusMonths(1);
		} else {
			periodFromOut = periodFrom;
			if (periodTo == null){
				periodToOut = periodFromOut.plusMonths(1);
			} else {
				periodToOut = periodTo;
			}
		}

		if ( !isDateOK(periodFromOut, periodToOut) ){
			throw new PDCException("date to cannot be smaller then date from");
		}

		return new LocalDateHolder(periodFromOut, periodToOut);
	}

	private boolean isDateOK(LocalDate periodFrom, LocalDate periodTo) {
		int compareSmaller = periodFrom.compareTo(periodTo);
		if (compareSmaller < 0) {
			return true;
		}
		return false;
	}
}
