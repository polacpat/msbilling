package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.springframework.stereotype.Service;

import com.tieto.tds.services.MathService;

@Service
public class MathServiceImpl implements MathService{

	@Override
	public BigDecimal getRoundedNumber(BigDecimal price) {
		return price.setScale(2, RoundingMode.HALF_DOWN);
	}

}
