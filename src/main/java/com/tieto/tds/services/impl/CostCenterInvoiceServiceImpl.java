package com.tieto.tds.services.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.CostCenterInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;
import com.tieto.tds.repositories.invoicing.CostCenterInvoiceRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.CostCenterInvoiceLineService;
import com.tieto.tds.services.CostCenterInvoiceService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CostCenterInvoiceServiceImpl implements CostCenterInvoiceService {

	private static final Logger log = LoggerFactory.getLogger(CompanyInvoiceServiceImpl.class);

	private CostCenterInvoiceLineService costCenterInvoiceLineService;

	private CostCenterInvoiceRepository costCenterInvoiceRepository;

	private InvoiceLineRepository invoiceLineRepository;

	private Mapper mapper;

	@Autowired
	public CostCenterInvoiceServiceImpl(CostCenterInvoiceLineService costCenterInvoiceLineService,
			CostCenterInvoiceRepository costCenterInvoiceRepository, InvoiceLineRepository invoiceLineRepository,
			Mapper mapper) {
		super();
		this.costCenterInvoiceLineService = costCenterInvoiceLineService;
		this.costCenterInvoiceRepository = costCenterInvoiceRepository;
		this.invoiceLineRepository = invoiceLineRepository;
		this.mapper = mapper;
	}

	@Override
	public CostCenterInvoiceDto createCostCenterInvoiceByDate(String companyid, String cctitle, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("generateInvoiceForCustomerArea {}, {}.", companyid, periodFrom, periodTo);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String invoiceNumber = periodFrom.format(formatter);//TODO: externalize to service

		List<InvoiceLineDto> saasInvoiceLines = costCenterInvoiceLineService.getSaaSInvoiceLines(companyid, cctitle, invoiceNumber );
		List<InvoiceLineDto> saasUserFeeInvoiceLines = costCenterInvoiceLineService.getSaaSUserFeeInvoiceLines(companyid, cctitle);

		CostCenterInvoiceDto invoice = new CostCenterInvoiceDto();
		invoice.addLines(saasInvoiceLines);
		invoice.addLines(saasUserFeeInvoiceLines);

		CostCenterInvoice costCenterInvoice = costCenterInvoiceRepository.findOneByNumberAndCostCenterId(invoiceNumber, cctitle);
		if (costCenterInvoice != null){
			costCenterInvoiceRepository.delete(costCenterInvoice);
		}

		costCenterInvoice = new CostCenterInvoice(companyid, cctitle, invoiceNumber);
		CostCenterInvoice savedCostCenterInvoice = costCenterInvoiceRepository.save(costCenterInvoice);

		for (InvoiceLineDto lineDto : invoice.getLines()) {
			InvoiceLine line = new InvoiceLine(costCenterInvoice, lineDto.getItemName(), lineDto.getUnit(), lineDto.getAmount(), lineDto.getPrice());
			invoiceLineRepository.save(line);
		}

		invoice.setId(savedCostCenterInvoice.getId());
		return invoice;
	}

	@Override
	public CostCenterInvoiceDto findOne(String companyid, String costCenterId, long id) {
		CostCenterInvoice invoice = costCenterInvoiceRepository.findOneById(id);
		return mapper.map(invoice, CostCenterInvoiceDto.class);
	}

	@Override
	public CostCenterInvoicePageDto findAllByCostCenter(String companyid, String costCenterId, PageableMs pageable) {
		Pageable pageableRequest = getPageable(pageable);
		Page<CostCenterInvoice> invoices = costCenterInvoiceRepository.findAllByCompanyidAndCostCenterId(companyid, costCenterId, pageableRequest);
		return mapInvoices(invoices);
	}

	@Override
	public CostCenterInvoicePageDto findAllByCompanyidAndNo(String companyid, String no, PageableMs pageable) {
		Pageable pageableRequest = getPageable(pageable);
		Page<CostCenterInvoice> invoices = costCenterInvoiceRepository.findAllByCompanyidAndNumber(companyid, no, pageableRequest);
		return mapInvoices(invoices);
	}

	private CostCenterInvoicePageDto mapInvoices(Page<CostCenterInvoice> projectInvoices) {
		CostCenterInvoicePageDto res = new CostCenterInvoicePageDto();
		List<CostCenterInvoiceDto> result = new ArrayList<>();
		for (CostCenterInvoice invoice : projectInvoices.getContent()) {
			CostCenterInvoiceDto invoiceDto = new CostCenterInvoiceDto();
			invoiceDto.setCompanyid(invoice.getCompanyid());
			invoiceDto.setCostCenterId( invoice.getCostCenterId());
			invoiceDto.setDateCreated( invoice.getDateCreated());
			invoiceDto.setId( invoice.getId());
			invoiceDto.setNumber( invoice.getNumber());
			List<InvoiceLineDto> lines = new ArrayList<>();
			if (invoice.getLines()!=null){
				for (InvoiceLine line : invoice.getLines()) {
					InvoiceLineDto lineDto = new InvoiceLineDto();
					lineDto.setDateCreated(line.getDateCreated());
					lineDto.setAmount( line.getAmount());
					lineDto.setId( line.getId());
					lineDto.setItemName( line.getItemName());
					lineDto.setLastUpdated( line.getLastUpdated());
					lineDto.setPrice( line.getPrice());
					lineDto.setUnit( line.getUnit());
					lines.add(lineDto);
				}
			}
			invoiceDto.setLines(lines);
			result.add(invoiceDto);
		}
		
		com.tieto.tds.dtos.common.Pageable pageResult = new com.tieto.tds.dtos.common.Pageable();
		pageResult.setLast(projectInvoices.isLast());
		pageResult.setNumber(projectInvoices.getNumber());
		pageResult.setNumberOfElements(projectInvoices.getNumberOfElements());
		pageResult.setSize(projectInvoices.getSize());
		pageResult.setTotalPages(projectInvoices.getTotalPages());
		res.setPageable(pageResult);
		res.setContent(result);
		
		return res;
	}

	@Override
	public CostCenterInvoicePageDto findAllByCompanyid(String companyid, PageableMs pageable) {
		Pageable pageableRequest = getPageable(pageable); 
		Page<CostCenterInvoice> projectInvoices = costCenterInvoiceRepository.findAllByCompanyid(companyid, pageableRequest );
		return mapInvoices(projectInvoices);
	}

	private Pageable getPageable(PageableMs pageable) {
		org.springframework.data.domain.Sort.Direction sortDirection = org.springframework.data.domain.Sort.Direction.DESC;
		if (pageable.getSort() == PageSort.ASC ){
			sortDirection = org.springframework.data.domain.Sort.Direction.ASC;
		}
		return new PageRequest(pageable.getPage()-1, pageable.getMax(), sortDirection, pageable.getOrderby());
	}
}
