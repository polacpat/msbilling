package com.tieto.tds.services.impl;

import java.time.LocalDate;
import java.util.Calendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.repositories.BackendRepository;
import com.tieto.tds.repositories.common.TokenHolder;
import com.tieto.tds.services.AdminService;
import com.tieto.tds.services.CronService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CronServiceImpl implements CronService{

	private static final Logger log = LoggerFactory.getLogger(CronServiceImpl.class);

	private static final long NEXT_AUTHENTICATION_TIME = 60000;

	private TokenHolder tokenHolder;

	private BackendRepository backendRepository;

	private AdminService adminService;
	@Autowired
	public CronServiceImpl(TokenHolder tokenHolder, BackendRepository backendRepository,
			AdminService adminService) {
		super();
		this.tokenHolder = tokenHolder;
		this.backendRepository = backendRepository;
		this.adminService = adminService;
	}

	@Override
	@Scheduled(fixedRate = NEXT_AUTHENTICATION_TIME)
	public void keepBackendSession() {
		long expiration = tokenHolder.getTokenExpiration();
		if ( isTokenExpired(expiration)){
			log.info("token1 {} will expire in {}", tokenHolder.getToken(), expiration);
			backendRepository.doLogin();
		}

	}

	@Override
	@Scheduled(cron = "0 0 2 1 * ?")
	public void generateInvoices() {
		log.debug("---- GENERATE INVOICE CRON JOB STARTED ----");
		LocalDate periodFrom = LocalDate.now().withDayOfMonth(1);
		LocalDate periodTo = periodFrom.withDayOfMonth(periodFrom.lengthOfMonth());
		adminService.generateInvoices(periodFrom, periodTo);
		log.debug("---- GENERATE INVOICE CRON JOB FINISHED ----");
	}

	public boolean isTokenExpired(long expiration){
		long timeToReauthenticate = Calendar.getInstance().getTimeInMillis() + NEXT_AUTHENTICATION_TIME;
		return timeToReauthenticate > expiration;
	}

}
