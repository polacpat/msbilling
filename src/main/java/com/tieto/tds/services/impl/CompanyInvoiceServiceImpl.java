package com.tieto.tds.services.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.domains.invoice.CompanyInvoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.invoicing.CompanyInvoiceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.invoicing.CompanyInvoiceRepository;
import com.tieto.tds.repositories.invoicing.InvoiceLineRepository;
import com.tieto.tds.services.CompanyInvoiceLineService;
import com.tieto.tds.services.CompanyInvoiceService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class CompanyInvoiceServiceImpl implements CompanyInvoiceService{

	private static final Logger log = LoggerFactory.getLogger(CompanyInvoiceServiceImpl.class);

	private CompanyInvoiceRepository companyInvoiceRepository;

	private InvoiceLineRepository invoiceLineRepository;

	private CompanyInvoiceLineService companyInvoiceLineService;

	private Mapper mapper;

	@Autowired
	public CompanyInvoiceServiceImpl(CompanyInvoiceRepository companyInvoiceRepository,
			InvoiceLineRepository invoiceLineRepository, CompanyInvoiceLineService companyInvoiceLineService,
			Mapper mapper) {
		super();
		this.companyInvoiceRepository = companyInvoiceRepository;
		this.invoiceLineRepository = invoiceLineRepository;
		this.companyInvoiceLineService = companyInvoiceLineService;
		this.mapper = mapper;
	}

	@Override
	public CompanyInvoiceDto createInvoice(String companyid, LocalDate periodFrom, LocalDate periodTo) {
		log.debug("generateInvoiceForCustomerArea {}, {}.", companyid, periodFrom, periodTo);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM");
		String invoiceNumber = periodFrom.format(formatter);//TODO: externalize to service

		List<InvoiceLineDto> projectsInvoiceLines = companyInvoiceLineService.getProjectsInvoiceLines(companyid, invoiceNumber );
		List<InvoiceLineDto> costCenterInvoiceLines = companyInvoiceLineService.getCostCenterInvoiceLines(companyid, invoiceNumber);
		List<InvoiceLineDto> saasInvoiceLines = companyInvoiceLineService.getSaaSInvoiceLines(companyid, invoiceNumber, periodFrom, periodTo);
		List<InvoiceLineDto> userFeeInvoiceLines = companyInvoiceLineService.getUserFeeInvoiceLines(companyid, periodFrom, periodTo);

		CompanyInvoiceDto invoice = new CompanyInvoiceDto();
		invoice.addLines(projectsInvoiceLines);
		invoice.addLines(costCenterInvoiceLines);
		invoice.addLines(saasInvoiceLines);
		invoice.addLines(userFeeInvoiceLines);

		CompanyInvoice companyInvoice = companyInvoiceRepository.findOneByNumberAndCompanyid(invoiceNumber, companyid);
		if (companyInvoice != null) companyInvoiceRepository.delete(companyInvoice);

		companyInvoice = new CompanyInvoice(companyid, invoiceNumber);
		CompanyInvoice savedProjectInvoice = companyInvoiceRepository.save(companyInvoice);

		for (InvoiceLineDto lineDto : invoice.getLines()) {
			InvoiceLine line = new InvoiceLine(companyInvoice, lineDto.getItemName(), lineDto.getUnit(), lineDto.getAmount(), lineDto.getPrice());
			invoiceLineRepository.save(line);
		}

		invoice.setId(savedProjectInvoice.getId());
		return invoice;

	}

	@Override
	public CompanyInvoiceDto findOne(String companyid, long id) {
		CompanyInvoice invoice = companyInvoiceRepository.findOneById(id);
		return mapper.map(invoice, CompanyInvoiceDto.class);
	}

	@Override
	public List<CompanyInvoiceDto> findAllByCompanyid(String companyid) {
		List<CompanyInvoice> invoices = companyInvoiceRepository.findAllByCompanyid(companyid);
		List<CompanyInvoiceDto>  result = new ArrayList<>();
		for (CompanyInvoice invoice : invoices) {
			result.add(mapper.map(invoice, CompanyInvoiceDto.class));
		}
		return result;
	}

}
