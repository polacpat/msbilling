package com.tieto.tds.services.impl;

import java.io.File;
import java.util.Date;

import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.configs.properties.MailProperties;
import com.tieto.tds.services.MailService;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class MailServiceImpl implements MailService{

	private static final Logger log = LoggerFactory.getLogger(MailServiceImpl.class);

	private static final String ADMIN_EMAIL = "pdcloudops@tieto.com";

	private JavaMailSender mailSender;

	private MailProperties properties;

	@Autowired
	public MailServiceImpl(JavaMailSender mailSender, MailProperties properties) {
		super();
		this.mailSender = mailSender;
		this.properties = properties;
	}


	@Override
	@Async
	public void sendBillingReport(final File file) {

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void prepare(MimeMessage mimeMessage) throws Exception {
				log.debug("Prepare invoice report email {} for user(s) {}.", file.getName(), properties.getReport().getReceivers());
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
				message.setTo( properties.getReport().getReceivers() );
				message.setFrom(new InternetAddress(ADMIN_EMAIL) );
				message.setSubject("Invoice report " + file.getName());
				message.setSentDate(new Date());

				message.addAttachment(file.getName(), file);

				message.setText("FYI", true);
			}
		};
		mailSender.send(preparator);
	}

}
