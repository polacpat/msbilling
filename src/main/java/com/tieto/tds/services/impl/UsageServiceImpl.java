package com.tieto.tds.services.impl;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalField;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.services.DateUtils;
import com.tieto.tds.services.UsageService;
import com.tieto.tds.services.commons.ResourceUsage;

@Service
@Profile(Constants.PROFILE_DEFAULT)
public class UsageServiceImpl implements UsageService{

	private static final Logger log = LoggerFactory.getLogger(UsageServiceImpl.class);

	@Override
	public ResourceUsage getTotalUsage(List<Usage> tenantUsage) {
		log.debug("getTotalUsage");
		Usage tempUsage = null;
		ResourceUsage resourceUsage = new ResourceUsage();

		for (Usage usage : tenantUsage) {
			if (tempUsage != null) {
				addUsageForPeriodBetweenTwoRecords(resourceUsage, usage, tempUsage);
			}
			tempUsage = usage;
		}
		addUsageForLastPeriod(resourceUsage, tempUsage);

		return resourceUsage;
	}

	/**
	 * add virtual resource usage to the input resourceUsage object based on the time in milliseconds during
	 * which that resource was used 
	 */
	private void addUsageForPeriodBetweenTwoRecords(ResourceUsage resourceUsage, Usage nextUsage, Usage currentUsage) {
		log.debug("addUsageForPeriodBetweenTwoRecords: {}, currentUsage: {}, nextUsage: {}", resourceUsage, currentUsage, nextUsage);

		long periodSizeInMillis = currentUsage.getTimestamp().until( nextUsage.getTimestamp(), ChronoUnit.SECONDS);
		log.debug("addUsageForPeriodBetweenTwoRecords from: {}, to: {}, millis: {}, hours: {}.",
				currentUsage.getTimestamp(), nextUsage.getTimestamp(), periodSizeInMillis, periodSizeInMillis/60/60/24);

		resourceUsage.addCpu(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getVcpu()));
		resourceUsage.addRam(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getVram())
				.divide(new BigDecimal(1024), 20, RoundingMode.HALF_UP));
		resourceUsage.addHdd(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getVdisk()));
		resourceUsage.addInstances(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getInstances()));
		resourceUsage.addImages(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getImages()));
		resourceUsage.addSnapshots(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getVolumeSnapshots()));
		resourceUsage.addVolumes(getSubTototalUsageInHours(periodSizeInMillis, currentUsage.getVolumes()));
	}

	/**
	 * add virtual resource usage to the input resourceUsage object based on the time in milliseconds which
	 * remain till the end of month
	 */
	private void addUsageForLastPeriod(ResourceUsage resourceUsage, Usage tempUsage) {
		log.debug("addUsageForLastPeriod: {}", resourceUsage, tempUsage);

		LocalDateTime end = tempUsage.getTimestamp().toLocalDate().withDayOfMonth(1).plusMonths(1).atStartOfDay();

		long periodSizeInMillis = tempUsage.getTimestamp().until(end, ChronoUnit.SECONDS);
		log.debug("addUsageForLastPeriod from: {}, to: {}, millis: {}.",
				tempUsage.getTimestamp(), end, periodSizeInMillis);

		resourceUsage.addCpu(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getVcpu()));
		resourceUsage.addRam(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getVram())
				.divide(new BigDecimal(1024), 20, RoundingMode.HALF_UP));
		resourceUsage.addHdd(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getVdisk()));
		resourceUsage.addInstances(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getInstances()));
		resourceUsage.addImages(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getImages()));
		resourceUsage.addSnapshots(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getVolumeSnapshots()));
		resourceUsage.addVolumes(getSubTototalUsageInHours(periodSizeInMillis, tempUsage.getVolumes()));
	}

	long ONE_HOUR = 3600;

	public BigDecimal getSubTototalUsageInHours(long period, long usage) {
		return new BigDecimal(usage).multiply(new BigDecimal(period).divide(new BigDecimal(ONE_HOUR), 20, RoundingMode.HALF_UP));
	}
}
