package com.tieto.tds.services.project;

import java.time.LocalDate;

public class ProjectInvoiceRequest {

	private String companyid;
	private String projectid;
	private LocalDate periodFrom;
	private LocalDate periodTo;

	public ProjectInvoiceRequest(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		super();
		this.companyid = companyid;
		this.projectid = projectid;
		this.periodFrom = periodFrom;
		this.periodTo = periodTo;
	}

	public String getCompanyid() {
		return companyid;
	}
	public String getProjectid() {
		return projectid;
	}
	public LocalDate getPeriodFrom() {
		return periodFrom;
	}
	public LocalDate getPeriodTo() {
		return periodTo;
	}

	@Override
	public String toString() {
		return String.format("cID: %s, pID: %s, from: %s, to: %s", getCompanyid(), getProjectid(), getPeriodFrom(), getPeriodTo());
	}
}
