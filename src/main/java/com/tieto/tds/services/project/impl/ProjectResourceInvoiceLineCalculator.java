package com.tieto.tds.services.project.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.Constants;
import com.tieto.tds.configs.properties.BackendProperties;
import com.tieto.tds.dtos.company.project.FeeDto;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.dtos.company.project.resource.ResourceDto;
import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.ProjectRepository;
import com.tieto.tds.repositories.ResourceRepository;
import com.tieto.tds.repositories.TenantUsageRepository;
import com.tieto.tds.services.UsageService;
import com.tieto.tds.services.commons.ResourceUsage;
import com.tieto.tds.services.project.ProjectInvoiceLineCalculator;
import com.tieto.tds.services.project.ProjectInvoiceRequest;

@Service("projectResourceInvoiceLineCalculator")
public class ProjectResourceInvoiceLineCalculator extends ProjectInvoiceLineCalculator{

	private static final Logger log = LoggerFactory.getLogger(ProjectResourceInvoiceLineCalculator.class);

	private ProjectRepository projectRepository;

	private ResourceRepository resourceRepository;

	private TenantUsageRepository tenantUsageRepository;

	private UsageService usageService;

	private BackendProperties backendProperties;

	@Autowired
	public ProjectResourceInvoiceLineCalculator(ProjectRepository projectRepository,
			TenantUsageRepository tenantUsageRepository, UsageService usageService,
			BackendProperties backendProperties, ResourceRepository resourceRepository) {
		super();
		this.projectRepository = projectRepository;
		this.tenantUsageRepository = tenantUsageRepository;
		this.usageService = usageService;
		this.backendProperties = backendProperties;
		this.resourceRepository = resourceRepository;
	}

	@Override
	public List<InvoiceLineDto> getProjectInvoiceLines(ProjectInvoiceRequest request) {
		log.debug("getProjectResourcesInvoiceLines entered {}.", request);
		List<InvoiceLineDto> result = new ArrayList<>();

		if (backendProperties.isUseorchestration()){
			ProjectDto project = projectRepository.findOneProject(request.getCompanyid(), request.getProjectid());
			List<ResourceDto> resources = resourceRepository.findAllByCompanyAndProject(request.getCompanyid(), request.getProjectid());
			FeeDto fee = project.getTier().getCurrentFee();

			if (resources != null) {
				for (ResourceDto resourceDto : resources) {
					String tenantid = resourceDto.getTenantid();

					List<Usage> tenantUsage = tenantUsageRepository.getUsageForTenantInTimePeriod(tenantid , request.getPeriodFrom(), request.getPeriodTo());
					ResourceUsage totalUsage = usageService.getTotalUsage(tenantUsage);
					logResources(tenantid, totalUsage);

					result.add( new InvoiceLineDto("CPU", Constants.INVOICE_UNIT_PCS, totalUsage.getCpu(), fee.getCpu()) );
					result.add( new InvoiceLineDto("RAM", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getRam(), fee.getRam()) );
					result.add( new InvoiceLineDto("HDD", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getHdd(), fee.getHdd()) );
					result.add( new InvoiceLineDto("Instances", Constants.INVOICE_UNIT_PCS, totalUsage.getInstances(), fee.getInstance()) );
					result.add( new InvoiceLineDto("Images", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getImages(), fee.getHdd()) );
					result.add( new InvoiceLineDto("Snapshots", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getSnapshots(), fee.getHdd()) );
					result.add( new InvoiceLineDto("Volumes", Constants.INVOICE_UNIT_GB_HOURS, totalUsage.getVolumes(), fee.getHdd()) );
				}
			}
		} else {
			log.debug("Orchestration is not available, project resource invoices are skipped.");
		}
		return result;
	}

	private void logResources(String tenantid, ResourceUsage resourceUsage){
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonInString = objectMapper.writeValueAsString(resourceUsage);
			log.debug("{} RETURNED: {}", tenantid, jsonInString);
		} catch (JsonProcessingException e) {
			log.warn("Error logging resources for tenantid {}.", tenantid, e);
		}
	}
}
