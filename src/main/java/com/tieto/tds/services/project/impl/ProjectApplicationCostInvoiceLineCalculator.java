package com.tieto.tds.services.project.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.repositories.ApplicationRepository;
import com.tieto.tds.services.project.ProjectInvoiceLineCalculator;
import com.tieto.tds.services.project.ProjectInvoiceRequest;

@Service("projectApplicationCostInvoiceLineCalculator")
public class ProjectApplicationCostInvoiceLineCalculator extends ProjectInvoiceLineCalculator {

	private static final Logger log = LoggerFactory.getLogger(ProjectApplicationCostInvoiceLineCalculator.class);

	private ApplicationRepository applicationRepository;

	@Autowired
	public ProjectApplicationCostInvoiceLineCalculator(ApplicationRepository applicationRepository) {
		super();
		this.applicationRepository = applicationRepository;
	}

	@Override
	public List<InvoiceLineDto> getProjectInvoiceLines(ProjectInvoiceRequest requet) {
		List<InvoiceLineDto> result = new ArrayList<InvoiceLineDto>();
		List<ApplicationDto> apps = applicationRepository.findAllActiveAppsByProjectidAndPeriod(requet.getCompanyid(), requet.getProjectid(), requet.getPeriodFrom(), requet.getPeriodTo());
		for (ApplicationDto app: apps){
			log.debug(app.getAppid() + " + " + app.getCostPeriod() + " for " + app.getCostPrice());
			InvoiceLineDto appInvoiceLine = new InvoiceLineDto();
			appInvoiceLine.setItemName( app.getStoreItem().getTitle() + " - " + app.getServer().getFqdn() );
			appInvoiceLine.setAmount(1d);
			appInvoiceLine.setPrice(BigDecimal.valueOf(app.getCostPrice()));
			appInvoiceLine.setUnit("pcs");
			result.add(appInvoiceLine);
		}
		return result;
	}

}
