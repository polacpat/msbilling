package com.tieto.tds.services;

import java.time.LocalDate;
import java.util.List;

import com.tieto.tds.dtos.invoicing.InvoiceLineDto;

public interface CompanyInvoiceLineService {

	public List<InvoiceLineDto> getProjectsInvoiceLines(String companyid, String no);

	public List<InvoiceLineDto> getCostCenterInvoiceLines(String companyid, String invoiceNumber);

	public List<InvoiceLineDto> getSaaSInvoiceLines(String companyid, String no, LocalDate periodFrom, LocalDate periodTo);

	public List<InvoiceLineDto> getUserFeeInvoiceLines(String companyid, LocalDate periodFrom, LocalDate periodTo);

}
