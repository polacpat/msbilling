package com.tieto.tds.services.reports;

public class ProjectInvoiceExportLine {

	private String no;
	private String projectid;
	private String companyid;
	private String itemname;
	private double amount;

	public ProjectInvoiceExportLine(String no, String projectid, String companyid, String itemname, double amount) {
		super();
		this.no = no;
		this.projectid = projectid;
		this.companyid = companyid;
		this.itemname = itemname;
		this.amount = amount;
	}
	public String getNo() {
		return no;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public String getProjectid() {
		return projectid;
	}
	public void setProjectid(String projectid) {
		this.projectid = projectid;
	}
	public String getItemname() {
		return itemname;
	}
	public void setItemname(String itemname) {
		this.itemname = itemname;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "no:"+no+", amount:"+amount;
	}
	public String getCompanyid() {
		return companyid;
	}
	public void setCompanyid(String companyid) {
		this.companyid = companyid;
	}
}
