package com.tieto.tds.services.reports;

import java.io.ByteArrayOutputStream;

public interface ExportInvoiceService {

	public ByteArrayOutputStream exportInvoicesByPeriod(String period);
}
