package com.tieto.tds.services.reports;

import java.util.List;

import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.company.project.aur.AurApp;

public interface ExportInvoiceHelperService {

	public double getUserCountForCC(List<InvoiceLine> lines);
	
	public double getAurUsersCount(AurApp aurApp, List<InvoiceLine> lines);
}
