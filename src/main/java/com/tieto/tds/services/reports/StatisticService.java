package com.tieto.tds.services.reports;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.tieto.tds.domains.invoice.line.InvoiceLine;
import com.tieto.tds.dtos.reporting.CapacityStatisticsDto;

public interface StatisticService {

	public Map<String, CapacityStatisticsDto> getAllProjectsUsageForPeriod(String no);

	public BigDecimal getTotalPrice(List<InvoiceLine> lines);
}
