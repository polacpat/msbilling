package com.tieto.tds.services;

import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.dtos.company.project.ProjectDto;

public interface UrlService {

	public String getProjectUrl(ProjectDto project);
	
	public String getCostCenterUrl(CostCenterDto costCenter);
	
	public String getCompanyUrl(CompanyDto company);
}
