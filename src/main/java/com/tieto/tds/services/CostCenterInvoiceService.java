package com.tieto.tds.services;

import java.time.LocalDate;

import com.tieto.tds.dtos.common.PageSort;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;

public interface CostCenterInvoiceService {

	public CostCenterInvoiceDto createCostCenterInvoiceByDate(String companyid, String costCenterId, LocalDate periodFrom, LocalDate periodTo);

	public CostCenterInvoiceDto findOne(String companyid, String costCenterId, long id);

	public CostCenterInvoicePageDto findAllByCostCenter(String companyid, String costCenterId, PageableMs pageable);

	public CostCenterInvoicePageDto findAllByCompanyidAndNo(String companyid, String no, PageableMs pageable);

	public CostCenterInvoicePageDto findAllByCompanyid(String companyid, PageableMs pageable);
}
