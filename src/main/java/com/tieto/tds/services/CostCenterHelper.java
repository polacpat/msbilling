package com.tieto.tds.services;

import com.tieto.tds.dtos.company.project.aur.AurApp;

public interface CostCenterHelper {

	public boolean isAplicableForCCInvoice(AurApp aurApp);

}
