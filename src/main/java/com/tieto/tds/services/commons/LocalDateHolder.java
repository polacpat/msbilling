package com.tieto.tds.services.commons;

import java.time.LocalDate;

public class LocalDateHolder {

	private LocalDate from;
	private LocalDate to;

	public LocalDateHolder(LocalDate from, LocalDate to) {
		super();
		this.from = from;
		this.to = to;
	}

	public LocalDate getFrom() {
		return from;
	}
	public LocalDate getTo() {
		return to;
	}
}
