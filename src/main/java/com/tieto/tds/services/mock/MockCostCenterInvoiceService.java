package com.tieto.tds.services.mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.common.Pageable;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.CostCenterInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.CostCenterInvoicePageDto;
import com.tieto.tds.services.CostCenterInvoiceService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockCostCenterInvoiceService implements CostCenterInvoiceService {

	@Override
	public CostCenterInvoiceDto createCostCenterInvoiceByDate(String companyid, String costCenterId,
			LocalDate periodFrom, LocalDate periodTo) {
		return new CostCenterInvoiceDto();
	}

	@Override
	public CostCenterInvoiceDto findOne(String companyid, String costCenterId, long id) {
		return new CostCenterInvoiceDto();
	}

	@Override
	public CostCenterInvoicePageDto findAllByCostCenter(String companyid, String costCenterId, PageableMs pageable) {
		return getCCInvoicePage();
	}

	@Override
	public CostCenterInvoicePageDto findAllByCompanyidAndNo(String companyid, String no, PageableMs pageable) {
		return getCCInvoicePage();
	}

	@Override
	public CostCenterInvoicePageDto findAllByCompanyid(String companyid, PageableMs pageable) {
		return getCCInvoicePage();
	}

	private CostCenterInvoicePageDto getCCInvoicePage(){
		CostCenterInvoicePageDto page = new CostCenterInvoicePageDto();
		CostCenterInvoiceDto invoice = new CostCenterInvoiceDto();

		List<CostCenterInvoiceDto> content = new ArrayList<>();
		content.add(invoice);
		Pageable pageablea = new Pageable();
		page.setPageable(pageablea);
		page.setContent(content);
		return page;
	}
}
