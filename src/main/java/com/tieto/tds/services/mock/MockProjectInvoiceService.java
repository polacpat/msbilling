package com.tieto.tds.services.mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.common.Pageable;
import com.tieto.tds.dtos.common.PageableMs;
import com.tieto.tds.dtos.invoicing.ProjectInvoiceDto;
import com.tieto.tds.dtos.invoicing.pages.ProjectInvoicePageDto;
import com.tieto.tds.services.ProjectInvoiceService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockProjectInvoiceService implements ProjectInvoiceService {

	private Map<String, ProjectInvoiceDto> map = new HashMap<String, ProjectInvoiceDto>();

	@Override
	public ProjectInvoiceDto createProjectInvoice(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		map.put(companyid+"-"+projectid, invoice);
		return invoice;
	}

	@Override
	public ProjectInvoiceDto findOne(String companyid, String projectid, long id) {
		ProjectInvoiceDto invoice = new ProjectInvoiceDto(); 
		return invoice;
	}

	@Override
	public ProjectInvoicePageDto findAllByProjectid(String companyid, String projectid, PageableMs pageable) {
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		Pageable pageablea = new Pageable();
		page.setPageable(pageablea);
		List<ProjectInvoiceDto> content = new ArrayList<ProjectInvoiceDto>();
		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		invoice.setCompanyid(companyid);
		invoice.setProjectid(projectid);
		invoice.setId(1);
		invoice.setNumber("2017-05");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		content.add(invoice);
		page.setContent(content);
		return page;
	}

	@Override
	public ProjectInvoicePageDto findAllByCompanyid(String companyid, String no, PageableMs pageable) {
		ProjectInvoicePageDto page = new ProjectInvoicePageDto();
		Pageable pageablea = new Pageable();
		page.setPageable(pageablea);
		List<ProjectInvoiceDto> content = new ArrayList<ProjectInvoiceDto>();
		ProjectInvoiceDto invoice = new ProjectInvoiceDto();
		invoice.setCompanyid(companyid);
		invoice.setProjectid("1234");
		invoice.setId(1);
		invoice.setNumber("2017-05");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		content.add(invoice);
		page.setContent(content);
		return page;
	}

}
