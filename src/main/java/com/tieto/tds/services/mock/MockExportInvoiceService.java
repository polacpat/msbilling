package com.tieto.tds.services.mock;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.exceptions.PDCException;
import com.tieto.tds.services.reports.ExportInvoiceService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockExportInvoiceService implements ExportInvoiceService {

	@Override
	public ByteArrayOutputStream exportInvoicesByPeriod(String period) {
		
		try (XSSFWorkbook workbook = new XSSFWorkbook()){
			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			workbook.write(stream);
			return stream;
		} catch (IOException e) {
			throw new PDCException("Cannot export usage for period: " + e);
		}
	}

}
