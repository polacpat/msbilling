package com.tieto.tds.services.mock;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.services.MigrationService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockMigrationService implements MigrationService {

	@Override
	public void migrateInvoices() {
		// TODO Auto-generated method stub
		
	}

}
