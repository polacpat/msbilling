package com.tieto.tds.services.mock;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.dtos.invoicing.CompanyInvoiceDto;
import com.tieto.tds.dtos.invoicing.InvoiceLineDto;
import com.tieto.tds.services.CompanyInvoiceService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockCompanyInvoiceService implements CompanyInvoiceService{

	CompanyInvoiceDto invoice;
	public MockCompanyInvoiceService(){
		invoice = new CompanyInvoiceDto();
		invoice.setCompanyid("111");
		invoice.setDateCreated(Calendar.getInstance().getTime());
		invoice.setId(1);
		invoice.setNumber("2017-05");
		List<InvoiceLineDto> lines = new ArrayList<InvoiceLineDto>();
		InvoiceLineDto line = new InvoiceLineDto();
		line.setAmount(100d);
		line.setDateCreated(Calendar.getInstance().getTime());
		line.setId(2);
		line.setItemName("TEST");
		line.setNo("2017-05");
		line.setPrice(new BigDecimal(300));
		line.setUnit("pcs");
		lines.add(line);
		invoice.setLines(lines);
	} 
	
	
	@Override
	public CompanyInvoiceDto createInvoice(String companyid, LocalDate periodFrom, LocalDate periodTo) {
		return invoice;
	}

	@Override
	public CompanyInvoiceDto findOne(String companyid, long id) {
		return invoice;
	}

	@Override
	public List<CompanyInvoiceDto> findAllByCompanyid(String companyid) {
		List<CompanyInvoiceDto> invoices = new ArrayList<CompanyInvoiceDto>();
		invoices.add(invoice);
		return invoices;
	}

}
