package com.tieto.tds.services.mock;

import java.time.LocalDate;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.tieto.tds.configs.Constants;
import com.tieto.tds.services.AdminService;

@Service
@Profile(Constants.PROFILE_MOCK)
public class MockAdminService implements AdminService {

	@Override
	public void getCustomJenkinsForInvoicing() {
		// Do nothing
	}

	@Override
	public void generateInvoices(LocalDate periodFrom, LocalDate periodTo) {
		// Do nothing
	}

}
