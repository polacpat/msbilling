package com.tieto.tds.services;

import java.time.LocalDate;

public interface AdminService {

	public void getCustomJenkinsForInvoicing();

	public void generateInvoices(LocalDate periodFrom, LocalDate periodTo);

}
