package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.company.project.resource.ResourceDto;

public interface ResourceRepository {

	public List<ResourceDto> findAllByCompanyAndProject(String companyid, String projectid);
}
