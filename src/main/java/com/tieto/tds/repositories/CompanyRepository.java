package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.company.CompanyDto;

public interface CompanyRepository {

	public CompanyDto findOneCompany(String companyid);

	public List<CompanyDto> findAll();

}
