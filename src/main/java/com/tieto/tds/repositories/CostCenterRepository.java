package com.tieto.tds.repositories;

import java.util.List;

import com.tieto.tds.dtos.company.CostCenterDto;

public interface CostCenterRepository {

	public CostCenterDto findOneCostCenter(String companyid, String ccTitle);

	public List<CostCenterDto> findAllCostCentersByCompany(String companyid);
}
