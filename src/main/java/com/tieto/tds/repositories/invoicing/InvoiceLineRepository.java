package com.tieto.tds.repositories.invoicing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tieto.tds.domains.invoice.Invoice;
import com.tieto.tds.domains.invoice.line.InvoiceLine;

public interface InvoiceLineRepository extends JpaRepository<InvoiceLine, Long>{

	List<InvoiceLine> findAllByInvoice(Invoice companyInvoice);

	List<InvoiceLine> findAllByInvoiceId(long id);

}
