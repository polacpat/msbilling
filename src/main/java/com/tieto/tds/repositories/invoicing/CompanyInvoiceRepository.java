package com.tieto.tds.repositories.invoicing;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.tieto.tds.domains.invoice.CompanyInvoice;

public interface CompanyInvoiceRepository extends JpaRepository<CompanyInvoice, Long>{

	public CompanyInvoice findOneByNumberAndCompanyid(String number, String companyid);

	public CompanyInvoice findOneById(long id);

	public List<CompanyInvoice> findAllByCompanyid(String companyid);

	public List<CompanyInvoice> findAllByNumber(String companyid);

}
