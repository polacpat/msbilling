package com.tieto.tds.repositories.common;

import java.io.IOException;
import java.nio.charset.Charset;

import org.apache.commons.codec.binary.Base64;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

public class BasicAurhInterceptor implements ClientHttpRequestInterceptor{

	private String password;

	private String username;

	public BasicAurhInterceptor(String username, String password) {
		super();
		this.password = password;
		this.username = username;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body,
			ClientHttpRequestExecution execution) throws IOException {

		request.getHeaders().add( "Authorization", getBasicAuthorization());
		request.getHeaders().add( "Transaction-id", String.valueOf(Thread.currentThread().getId()));
		ClientHttpResponse response = execution.execute(request, body);

		return response;
	}

	private String getBasicAuthorization(){
		final String auth = username + ":" + password;
		final byte[] encodedAuth = Base64.encodeBase64( auth.getBytes( Charset.forName( "US-ASCII" ) ) );
		final String authHeader = "Basic " + new String( encodedAuth );
		return authHeader;
	}

}
