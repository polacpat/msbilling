package com.tieto.tds.repositories.common;

public interface TokenHolder {

	public void setLoginResponse(LoginResponse loginResponse);

	public String getToken();

	public long getTokenExpiration();
}
