package com.tieto.tds.repositories.common;

public class TokenHolderImpl implements TokenHolder {

	private LoginResponse loginResponse;

	@Override
	public void setLoginResponse(LoginResponse loginResponse){
		this.loginResponse = loginResponse;
	}

	@Override
	public String getToken() {
		if (this.loginResponse==null)return "";
		return this.loginResponse.getToken();
	}

	@Override
	public long getTokenExpiration() {
		if (this.loginResponse == null)return 0;
		return this.loginResponse.getExpiration().getTime();
	}

}
