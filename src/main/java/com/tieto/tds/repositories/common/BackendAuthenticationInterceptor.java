package com.tieto.tds.repositories.common;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import com.tieto.tds.security.Originator;

public class BackendAuthenticationInterceptor implements ClientHttpRequestInterceptor{

	private static final Logger log = LoggerFactory.getLogger(BackendAuthenticationInterceptor.class);

	public static final String BACKEND_USER_IDENTIFICATION = "label";

	private Originator originatorService;

	private TokenHolder tokenHolder;

	@Autowired
	public BackendAuthenticationInterceptor(Originator originator, TokenHolder tokenHolder) {
		super();
		this.originatorService = originator;
		this.tokenHolder = tokenHolder;
	}

	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution ) throws IOException {
		// "label" is not null if it was set directly in concreet exchange call
		if ( request.getHeaders().get(BACKEND_USER_IDENTIFICATION) == null ){
			String userToken="";
			try {
				userToken = originatorService.getOriginator();
			} catch (Exception e){
				log.warn("Originator is not available in current request scope. ", e);
				// if interceptor is call outside of session scope token is not available. It should be passed in concreet exchange call.
			}
			request.getHeaders().add(BACKEND_USER_IDENTIFICATION, userToken);
		}
		log.debug("using token: " + tokenHolder.getToken());;
		request.getHeaders().add("Authentication", tokenHolder.getToken());

		return execution.execute(request, body);
	}

}
