package com.tieto.tds.repositories.impls;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.common.CostCenterPageable;
import com.tieto.tds.dtos.company.CostCenterDto;
import com.tieto.tds.repositories.CostCenterRepository;

@Repository
public class CostCenterRepositoryImpl implements CostCenterRepository{

	private static final Logger log = LoggerFactory.getLogger(ProjectRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public CostCenterRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}

	@Override
	public CostCenterDto findOneCostCenter(String companyid, String ccTitle) {
		String url = getOneCostCentersUrl(companyid, ccTitle);

		return restTemplate.getForObject(url, CostCenterDto.class);
	}

	@Override
	public List<CostCenterDto> findAllCostCentersByCompany(String companyid) {
		String url = getAllCostCentersUrl(companyid);

		CostCenterPageable projects = restTemplate.getForObject(url, CostCenterPageable.class);
		return projects.getContent();
	}

	private String getOneCostCentersUrl(String companyid, String ccTitle) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/costcenters/{ccTitle}").buildAndExpand(companyid, ccTitle);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	private String getAllCostCentersUrl(String companyid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/costcenters")
				.query("max={max}").buildAndExpand(companyid, 10000);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

}
