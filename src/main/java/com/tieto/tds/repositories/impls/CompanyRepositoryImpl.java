package com.tieto.tds.repositories.impls;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.CompanyDto;
import com.tieto.tds.repositories.CompanyRepository;

@Repository
public class CompanyRepositoryImpl implements CompanyRepository{

	private static final Logger log = LoggerFactory.getLogger(CompanyRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public CompanyRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}

	@Override
	@Cacheable(value = "company", key = "#companyid")
	public CompanyDto findOneCompany(String companyid) {
		String url = getOneCompanyUrl(companyid);

		return restTemplate.getForObject(url, CompanyDto.class);
	}

	@Override
	public List<CompanyDto> findAll() {
		String url = getAllCompanyUrl();

		CompanyDto[] companies = restTemplate.getForObject(url, CompanyDto[].class);
		return Arrays.asList(companies);
	}

	private String getAllCompanyUrl() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/admin/companies").build();
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	private String getOneCompanyUrl(String companyid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}").buildAndExpand(companyid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

}
