package com.tieto.tds.repositories.impls;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.project.ProjectDto;
import com.tieto.tds.repositories.ProjectRepository;

@Repository
public class ProjectRepositoryImpl implements ProjectRepository{

	private static final Logger log = LoggerFactory.getLogger(ProjectRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public ProjectRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}

	@Override
	@Cacheable(value = "project", key = "#projectid")
	public ProjectDto findOneProject(String companyid, String projectid) {
		String url = getOneProjectUrl(companyid, projectid);

		return restTemplate.getForObject(url, ProjectDto.class);
	}

	private String getOneProjectUrl(String companyid, String projectid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{areaid}/products/{projectid}").buildAndExpand(companyid, projectid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	@Override
	public List<ProjectDto> findAllProjectsByCompany(String companyid) {
		String url = getAllProjectsUrl(companyid);

		ProjectDto[] projects = restTemplate.getForObject(url, ProjectDto[].class);
		return Arrays.asList(projects);
	}

	private String getAllProjectsUrl(String companyid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/admin/companies/{companyid}/projects").buildAndExpand(companyid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

}
