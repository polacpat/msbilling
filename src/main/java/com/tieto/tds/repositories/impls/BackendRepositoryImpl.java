package com.tieto.tds.repositories.impls;

import java.util.Calendar;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.BackendProperties;
import com.tieto.tds.dtos.BackendLogin;
import com.tieto.tds.repositories.BackendRepository;
import com.tieto.tds.repositories.common.LoginResponse;
import com.tieto.tds.repositories.common.TokenHolder;

@Repository
public class BackendRepositoryImpl implements BackendRepository{

	private static final Logger log = LoggerFactory.getLogger(BackendRepositoryImpl.class);

	private BackendProperties properties;

	private RestTemplate restTemplate;

	private TokenHolder tokenHolder;

	private BackendProperties backendProperties;

	public BackendRepositoryImpl(BackendProperties properties, TokenHolder tokenHolder,
			@Qualifier("backendRestTemplate") RestTemplate restTemplate,
			BackendProperties backendProperties) {
		super();
		this.properties = properties;
		this.restTemplate = restTemplate;
		this.tokenHolder = tokenHolder;
		this.backendProperties = backendProperties;
	}	

	@Override
	public String doLogin() {
		if (backendProperties.isUseorchestration()){
			BackendLogin credential = getBackendCredentials();
			String url = getLoginUrl();
			LoginResponse res = restTemplate.postForObject(url, credential, LoginResponse.class);
			log.debug("Login expiration: " + res.getExpiration() +", token: "+ res.getToken());
			tokenHolder.setLoginResponse(res);
			return res.getToken();
		} else {
			//TODO: replace with backend profile to load completely different implementations
			LoginResponse res = new LoginResponse();
			Calendar cal = Calendar.getInstance();
			cal.set(2100, 1, 1);
			res.setExpiration(cal.getTime());
			tokenHolder.setLoginResponse(res );
			return "";
		}
	}

	private String getLoginUrl() {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/backend/login").build();

		log.info( "{}", uriComponents );

		return uriComponents.toUriString();
	}

	BackendLogin credentials = null;
	private BackendLogin getBackendCredentials() {
		if (credentials==null) credentials = new BackendLogin(properties.getUsername(), properties.getPassword());
		return credentials;
	}


}
