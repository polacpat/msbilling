package com.tieto.tds.repositories.impls;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.common.Pageable;
import com.tieto.tds.dtos.common.UserPageable;
import com.tieto.tds.dtos.company.project.aur.AurApp;
import com.tieto.tds.dtos.user.UserDto;
import com.tieto.tds.repositories.AurRepository;

@Repository
public class AurRepositoryImpl implements AurRepository {

	private static final Logger log = LoggerFactory.getLogger(AurRepositoryImpl.class);

	@Autowired
	@Qualifier("portalRestTemplate") 
	private RestTemplate restTemplate;
	
	@Autowired
	private PortalProperties properties;

	@Override
	public List<UserDto> findAllUsersByCostcenterAndAppid(String companyid, String costCenterId, String appid) {
		String url = getfindAllUsersByCostCenterAndAppidUrl(companyid,costCenterId,appid);
		UserDto[] users =  restTemplate.getForObject(url, UserDto[].class);
		return Arrays.asList(users);
	}

	@Override
	public List<UserDto> findAllUsersByProjectAndAppid(String companyid, String projectid, String appid) {
		String url = getfindAllUsersByAppidUrl(companyid, projectid, appid);
		UserPageable users =  restTemplate.getForObject(url, UserPageable.class);
		return users.getContent();
	}

	@Override
	public List<AurApp> findAllAurAppsByCompanyid(String companyid) {
		String url = getAllAurApsByCompanyUrl(companyid);
		AurApp[] apps =  restTemplate.getForObject(url, AurApp[].class);
		return Arrays.asList(apps);
	}

	private String getfindAllUsersByCostCenterAndAppidUrl(String companyid, String costcenterid, String appid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{areaid}/costcenters/{costcenterid}/saas/{appid}/users").buildAndExpand(companyid, costcenterid, appid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	private String getAllAurApsByCompanyUrl(String companyid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/saas").buildAndExpand(companyid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

	private String getfindAllUsersByAppidUrl(String companyid, String projectid, String appid) {
		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{companyid}/products/{projectid}/instances/{appid}/aur/appusers?max=10000").buildAndExpand(companyid, projectid, appid);
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}

}
