package com.tieto.tds.repositories.impls;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tieto.tds.configs.properties.BackendProperties;
import com.tieto.tds.dtos.company.project.resource.Usage;
import com.tieto.tds.repositories.TenantUsageRepository;

@Repository
public class TenantUsageRepositoryImpl implements TenantUsageRepository{

	private static final Logger log = LoggerFactory.getLogger(TenantUsageRepositoryImpl.class);

	private RestTemplate restTemplate;

	private BackendProperties properties;

	@Autowired
	public TenantUsageRepositoryImpl(@Qualifier("backendRestTemplate") RestTemplate restTemplate, BackendProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}

	@Override
	public List<Usage> getUsageForTenantInTimePeriod(String tenantid, LocalDate dateFrom, LocalDate dateTo) {
		String url = getRersourceUsageUrl(tenantid, dateFrom, dateTo);

		Usage[] result = restTemplate.getForObject(url, Usage[].class);
		logUsage(url, result);
		return Arrays.asList(result);
	}

	private void logUsage(String url, Usage[] result) {
		try {
			ObjectMapper objectMapper = new ObjectMapper();
			String jsonInString = objectMapper.writeValueAsString(result);
			log.debug("{} RETURNED: {}", url, jsonInString);
		} catch (JsonProcessingException e) {
			log.warn("Error logging usage.", e);
		}
	}

	private String getRersourceUsageUrl(String tenantid, LocalDate dateFrom, LocalDate dateTo) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		//from=00:00:00%2030/04/2017&to=23:59:59%2030/05/2017

		UriComponents uriComponents = UriComponentsBuilder.newInstance()
			      .scheme(properties.getSchema()).host(properties.getFqdn())
			      .path("/tenants/{tenantid}/usage").query("from=00:00:00 {from}&to=00:00:00 {to}").buildAndExpand(tenantid, dateFrom.format(formatter), dateTo.format(formatter));
		log.info("{}",uriComponents);
		
		return uriComponents.toUriString();
	}

}
