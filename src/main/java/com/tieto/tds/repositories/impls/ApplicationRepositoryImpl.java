package com.tieto.tds.repositories.impls;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.tieto.tds.configs.properties.BackendProperties;
import com.tieto.tds.configs.properties.PortalProperties;
import com.tieto.tds.dtos.company.project.app.ApplicationDto;
import com.tieto.tds.repositories.ApplicationRepository;

@Repository
public class ApplicationRepositoryImpl implements ApplicationRepository{

	private static final Logger log = LoggerFactory.getLogger(ApplicationRepositoryImpl.class);

	private RestTemplate restTemplate;

	private PortalProperties properties;

	@Autowired
	public ApplicationRepositoryImpl(@Qualifier("portalRestTemplate") RestTemplate restTemplate, PortalProperties properties) {
		super();
		this.restTemplate = restTemplate;
		this.properties = properties;
	}
	
	@Override
	public List<ApplicationDto> findAllActiveAppsByProjectidAndPeriod(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		String url = getAllAppsByProjectidUrl(companyid, projectid, periodFrom, periodTo);
		ApplicationDto[] apps = restTemplate.getForObject(url, ApplicationDto[].class);
		return Arrays.asList(apps);
	}

	private String getAllAppsByProjectidUrl(String companyid, String projectid, LocalDate periodFrom, LocalDate periodTo) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		UriComponents uriComponents = UriComponentsBuilder.newInstance()
				.scheme(properties.getSchema()).host(properties.getFqdn())
				.path("/rest-api/areas/{areaid}/products/{projectid}/apps")
				.query("from={from}&to={to}")
				.buildAndExpand(companyid, projectid, periodFrom.format(formatter), periodTo.format(formatter));
		log.info("{}",uriComponents);

		return uriComponents.toUriString();
	}
}
